import express, { Express } from "express";
import { json } from "body-parser";
import helmet from "helmet";
import compression from "compression";
import mongoose from "mongoose";
import { router } from "../routes";
import { environment } from "../environment";
import cors from "cors";

class Server {
  constructor(dbURL: string) {
    this.express = express();
    this.dbURL = dbURL;
  }

  public express: Express;

  public dbURL: string;

  private startExpress() {
    this.express.use(json({ limit: "100kb" }));
    this.express.use(helmet());
    this.express.use(compression());
    this.express.use(cors());

    this.express.use("/uploads", express.static("uploads"));

    this.express.use((req, res, next) => {
      res.header("Access-Control-Allow-Headers", "Content-Range");
      res.header("Access-Control-Allow-Origin", "*");
      next();
    });
    this.express.get("/testing", (req, res) => {
      res.send({ health: "ok!" });
    });

    this.express.use(router);

    this.express.listen(environment.port, () => {
      console.log(`🚀 Server ready at http://localhost:${environment.port}`);
    });
  }

  private async connectDB() {
    mongoose.set("debug", true);
    await mongoose.connect(this.dbURL, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });
  }

  public async start() {
    try {
      await this.connectDB();
      this.startExpress();
    } catch (e) {
      console.log(e);
    }
  }
}

export { Server };
