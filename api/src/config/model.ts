/* eslint-disable @typescript-eslint/no-unused-vars */
/* eslint-disable @typescript-eslint/no-empty-function */
import {
  DocumentDefinition,
  FilterQuery,
  Model as MongooseModel,
  QueryOptions,
  UpdateQuery,
} from "mongoose";
import { DB, TDocuments } from "../models";
import {
  CreateError,
  DeleteError,
  NotFoundError,
  UpdateError,
} from "../utils/errors";

export class Model<T extends TDocuments> {
  public model: MongooseModel<T>;

  public modelName: keyof DB;

  constructor(model: MongooseModel<T>, modelName: keyof DB) {
    this.model = model;
    this.modelName = modelName;
  }

  public async afterCreate({ doc }: { doc: T }) {}

  public async create({
    value,
  }: {
    value:
      | DocumentDefinition<T & { createdAt: never; updatedAt: never }>
      | DocumentDefinition<
          T & { createdAt: never; updatedAt: never; _id: never }
        >;
  }): Promise<T> {
    try {
      const doc = await this.model.create<T>({
        ...value,
      } as any);
      await this.afterCreate({ doc });

      return doc;
    } catch (e) {
      console.error("CreateError", e);
      throw new CreateError(this.modelName);
    }
  }

  public async createMany({
    values,
  }: {
    values: T & { createdAt: never; updatedAt: never }[];
  }) {
    try {
      const createdValues = await this.model.insertMany(values);

      return createdValues;
    } catch (e) {
      console.error("CreateError", e);
    }
  }

  public async afterUpdate({ doc }: { doc: T }) {}

  public async update({
    filter,
    update,
    options,
  }: {
    filter: FilterQuery<T>;
    update: UpdateQuery<T>;
    options?: QueryOptions;
  }): Promise<T> {
    try {
      const doc = await this.model.findOneAndUpdate(filter, update, {
        ...{
          new: true,
          runValidators: true,
        },
        ...options,
      });
      if (doc) {
        await this.afterUpdate({ doc });
        return doc;
      }
      throw new UpdateError(this.modelName);
    } catch (e) {
      console.error("UpdateError", e);
      throw new UpdateError(this.modelName);
    }
  }

  public async afterDelete({ doc }: { doc: T }) {}

  public async delete({ filter }: { filter: FilterQuery<T> }): Promise<T> {
    try {
      const doc = await this.model.findOneAndDelete(filter);
      if (doc !== null) {
        await this.afterDelete({ doc });
        return doc;
      }
      throw new DeleteError(this.modelName);
    } catch (e) {
      console.error("DeleteError", e);
      throw new DeleteError(this.modelName);
    }
  }

  public async deleteManyWithRes({
    filter,
  }: {
    filter: FilterQuery<T>;
  }): Promise<T[]> {
    try {
      const data = await this.model.find(filter);
      const { ok } = await this.model.deleteMany({ _id: data });
      if (ok === 1) {
        return data;
      }
      throw new DeleteError(this.modelName);
    } catch (e) {
      console.error("DeleteError", e);
      throw new DeleteError(this.modelName);
    }
  }

  public async deleteMany({
    filter,
  }: {
    filter: FilterQuery<T>;
  }): Promise<boolean> {
    try {
      const { ok } = await this.model.deleteMany(filter);
      if (ok === 1) {
        return true;
      }
      throw new DeleteError(this.modelName);
    } catch {
      throw new DeleteError(this.modelName);
    }
  }

  public async find({
    filter,
    projection,
    options = {},
    limit,
  }: {
    filter: FilterQuery<T>;
    limit?: number;
    projection?: keyof T[];
    options?: QueryOptions;
  }): Promise<T[]> {
    try {
      let newOptions: QueryOptions = { sort: { updatedAt: -1 } };
      if (limit) newOptions = { ...newOptions, ...options, limit };

      const data = await this.model.find(filter, projection, newOptions);
      return data;
    } catch (e) {
      console.error("NotFoundError", e);
      throw new NotFoundError(this.modelName);
    }
  }

  public async findOne({ filter }: { filter: FilterQuery<T> }): Promise<T> {
    try {
      const data = await this.model.findOne(filter);

      if (data) {
        return data;
      }
      throw new NotFoundError(this.modelName);
    } catch (e) {
      console.error("NotFoundError", e);
      throw new NotFoundError(this.modelName);
    }
  }

  public async count(): Promise<number> {
    try {
      const res = await this.model.count();
      return res;
    } catch (e) {
      console.error("CreateError", e);
      throw new CreateError(this.modelName);
    }
  }
}
