import { Request, Response } from "express";
import { Types } from "mongoose";
import { db } from "../models";

export const post = async (req: Request, res: Response) => {
  try {
    const data = await db.addition.create({
      value: { ...req.body },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const patch = async (req: Request, res: Response) => {
  try {
    const data = await db.addition.update({
      update: req.body,
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    const data = await db.addition.delete({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
