import { Request, Response } from "express";
import { Types } from "mongoose";
import { db } from "../models";

export const post = async (req: Request, res: Response) => {
  try {
    const count = await db.additionCategory.count();
    const data = await db.additionCategory.create({
      value: { ...req.body, position: count + 1 },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const patch = async (req: Request, res: Response) => {
  try {
    const data = await db.additionCategory.update({
      update: req.body,
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const all = async (req: Request, res: Response) => {
  try {
    const data = await db.additionCategory.getAll();

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    await db.addition.deleteMany({
      filter: { category: Types.ObjectId(req.params.id) },
    });
    const data = await db.additionCategory.delete({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
