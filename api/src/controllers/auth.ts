import { Request, Response } from "express";
import { db } from "../models";
import bcrypt from "bcrypt";
import { generateToken } from "../utils/auth";
import { AuthError } from "../utils/errors";

export const login = async (req: Request, res: Response) => {
  try {
    const data = await db.user.findOne({
      filter: { name: req.body.name },
    });

    const auth = await bcrypt.compare(req.body.password, data.password);
    if (auth) {
      const token = generateToken(data._id.toHexString());
      return res.status(200).send({ token });
    } else {
      throw new AuthError();
    }
  } catch (e) {
    return res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
