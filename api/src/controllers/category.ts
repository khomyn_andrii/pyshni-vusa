import { Request, Response } from "express";
import { Types } from "mongoose";
import { db } from "../models";

export const all = async (req: Request, res: Response) => {
  try {
    const data = await db.category.find({
      filter: {},
      options: {
        sort: { position: 1 },
      },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const post = async (req: Request, res: Response) => {
  try {
    const count = await db.category.count();
    const data = await db.category.create({
      value: { ...req.body, position: count + 1 },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const patch = async (req: Request, res: Response) => {
  try {
    const data = await db.category.update({
      update: req.body,
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    await db.product.deleteMany({
      filter: { category: Types.ObjectId(req.params.id) },
    });
    const data = await db.category.delete({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const byId = async (req: Request, res: Response) => {
  try {
    const data = await db.category.findOne({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
