import { Request, Response } from "express";
import { db } from "../models";

export const all = async (req: Request, res: Response) => {
  try {
    const data = await db.order.getCustomers({
      skip: isNaN(Number(req.query.skip)) ? 0 : Number(req.query.skip),
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
