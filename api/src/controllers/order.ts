import { Request, Response } from "express";
import { FilterQuery, QueryOptions, Types } from "mongoose";
import { db } from "../models";
import { TOrder } from "../models/Order";
import { OrderStatusEnum } from "../models/Order/types";
import fetch from "node-fetch";
import moment from "moment";
import { environment } from "../environment";

const chat = environment.telegramChat;
const token = environment.telegramBot;

export const all = async (req: Request, res: Response) => {
  try {
    const options: QueryOptions = { sort: { date: -1 } };

    const skip = isNaN(Number(req.query.skip))
      ? undefined
      : Number(req.query.skip);

    if (skip) {
      options.skip = skip;
    }

    const data = await db.order.find({
      filter: {
        status: {
          $in: [OrderStatusEnum.canceled, OrderStatusEnum.completed],
        },
      },
      options,
      limit: 15,
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const find = async (req: Request, res: Response) => {
  try {
    const key = isNaN(Number(req.query.text))
      ? undefined
      : Number(req.query.text);

    const text = req.query.text as string;

    const filter: FilterQuery<TOrder> = {
      $or: [
        {
          customerPhone: {
            $regex: text.replace(/[^0-9]/gim, ""),
            $options: "i",
          },
        },
      ],
    };

    if (key && filter.$or) {
      filter.$or.push({ key });
    }

    const data = await db.order.find({
      filter,
      options: { sort: { date: -1 } },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const allLive = async (req: Request, res: Response) => {
  try {
    const data = await db.order.find({
      filter: {
        status: {
          $in: [OrderStatusEnum.accepted, OrderStatusEnum.new],
        },
      },
      options: { sort: { date: -1 } },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const post = async (req: Request, res: Response) => {
  try {
    const count = await db.order.count();
    req.body.customerPhone = req.body.customerPhone.replace(/[^0-9]/gim, "");

    const data = await db.order.create({
      value: { ...req.body, key: count + 1 },
    });

    try {
      const jsonRes = await fetch(
        `https://api.telegram.org/bot${token}/sendMessage`,
        {
          method: "POST",
          body: JSON.stringify({
            chat_id: chat,
            text: `Заказ №: ${data.key}
Доставка: ${data.deliveryType === "delivery" ? "курьером" : "самовывоз"}
${data.deliveryType === "delivery" ? `Адрес доставки: ${data.address}` : ""}
Время: ${moment(data.date).format("YYYY/MM/DD HH:mm")}
Оплата: ${data.paymentType === "card" ? "картой" : "наличные"}
${data.changeFrom ? `Сдача с: ${data.changeFrom}` : ""}
Имя: ${data.customerName}
Телефон: ${data.customerPhone}
${data.callback ? "Нужно перезвонить" : "Перезванивать не обязательно"}
${data.customerEmail ? `Email: ${data.customerEmail}` : ""}
${data.comment ? `Комментарий: ${data.comment}` : ""}
__________________________________________________________
Товары:${data.products.map(
              (item) =>
                `

${item.quantity}x ${item.product.title}
${item.choosenSize.title} - ${item.choosenSize.diameter} см
${
  item.additions.length > 0
    ? `Дополнительно:
${item.additions.map((addiion) => `${addiion.title} - x${addiion.quantity}`)}`
    : ""
}`
            )}

${`Сумма:  ${data.sum} грн.`}`,
            reply_markup: {
              inline_keyboard: [
                [
                  {
                    text: "Отменить",
                    callback_data: `{"id":"${data._id}","type": "canceled"}`,
                  },
                  {
                    text: "Подтвердить",
                    callback_data: `{"id":"${data._id}","type": "accepted"}`,
                  },
                ],
              ],
            },
          }),
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      const res = await jsonRes.json();

      await db.order.update({
        filter: { _id: Types.ObjectId(data._id) },
        update: { telegramMessageId: res.result.message_id },
      });
    } catch {}

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const patch = async (req: Request, res: Response) => {
  try {
    const data = await db.order.update({
      update: req.body,
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    try {
      if (data.status === "accepted") {
        await fetch("https://api.turbosms.ua/message/send.json", {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${environment.smsToken}`,
          },
          body: JSON.stringify({
            recipients: [data.customerPhone],
            sms: {
              sender: "Dostavka24",
              text: `Пишні Вуса
Ваше замовлення №${data.key} принято.`,
            },
          }),
        });
      }
    } catch {}

    try {
      if (
        data.telegramMessageId &&
        (data.status === "new" || data.status === "accepted")
      ) {
        await fetch(
          `https://api.telegram.org/bot${token}/editMessageReplyMarkup`,
          {
            method: "POST",
            body: JSON.stringify({
              chat_id: chat,
              message_id: data.telegramMessageId,
              reply_markup: {
                inline_keyboard: [
                  [
                    {
                      text: "Отменить",
                      callback_data: `{"id":"${data._id}","type": "canceled"}`,
                    },
                    data.status === "new"
                      ? {
                          text: "Подтвердить",
                          callback_data: `{"id":"${data._id}","type": "accepted"}`,
                        }
                      : {
                          text: "Завершить",
                          callback_data: `{"id":"${data._id}","type": "completed"}`,
                        },
                  ],
                ],
              },
            }),
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
      } else {
        await fetch(
          `https://api.telegram.org/bot${token}/editMessageReplyMarkup`,
          {
            method: "POST",
            body: JSON.stringify({
              chat_id: chat,
              message_id: data.telegramMessageId,
              reply_markup: {},
            }),
            headers: {
              "Content-Type": "application/json",
            },
          }
        );
      }
    } catch {}

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    const data = await db.order.delete({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    try {
      await fetch(`https://api.telegram.org/bot${token}/deleteMessage`, {
        method: "POST",
        body: JSON.stringify({
          chat_id: chat,
          message_id: data.telegramMessageId,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });
    } catch {}

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const byId = async (req: Request, res: Response) => {
  try {
    const data = await db.order.findOne({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
