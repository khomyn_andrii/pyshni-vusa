import { Request, Response } from "express";
import { FilterQuery, Types } from "mongoose";
import { db, TProduct } from "../models";

export const all = async (
  req: Request<{}, any, {}, { category: string }>,
  res: Response
) => {
  try {
    const filter: FilterQuery<TProduct> = {};
    if (req.query.category) {
      filter.category = Types.ObjectId(req.query.category);
    }

    const data = await db.product.find({
      filter,
      options: { sort: { createAt: -1 } },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const post = async (req: Request, res: Response) => {
  try {
    const data = await db.product.create({ value: req.body });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const patch = async (req: Request, res: Response) => {
  try {
    const data = await db.product.update({
      update: req.body,
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const destroy = async (req: Request, res: Response) => {
  try {
    const data = await db.product.delete({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const byId = async (req: Request, res: Response) => {
  try {
    const data = await db.product.findOne({
      filter: { _id: Types.ObjectId(req.params.id) },
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
