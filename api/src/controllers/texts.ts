import { Request, Response } from "express";
import { db } from "../models";

export const get = async (req: Request, res: Response) => {
  try {
    const data = await db.texts.findOne({
      filter: {},
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};

export const patch = async (req: Request, res: Response) => {
  try {
    const data = await db.texts.update({
      update: req.body,
      filter: {},
    });

    res.status(200).send(data);
  } catch (e) {
    res
      .status(e.status || 500)
      .send({ error: { where: e.where, name: e.name, message: e.message } });
  }
};
