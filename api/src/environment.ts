/* eslint-disable import/first */
/* eslint-disable import/no-extraneous-dependencies */
require("dotenv").config();

export const environment = {
  db: {
    user: process.env.DB_USER as string,
    password: process.env.DB_PASSWORD as string,
    cluster: process.env.DB_CLUSTER as string,
  },
  port: process.env.PORT as string,
  jwt: {
    secret: process.env.JWT_SECRET as string,
    duration: process.env.JWT_DURATION as string,
  },
  smsToken: process.env.SMS_TOKEN as string,
  telegramChat: Number(process.env.TELEGRAM_CHAT) as number,
  telegramBot: process.env.TELEGRAM_BOT as string,
};
