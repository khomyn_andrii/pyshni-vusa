import { Server } from "./config/init";
// import { environment } from "./environment";

// const MONGODB_URL = `mongodb+srv://${environment.db.user}:${environment.db.password}@${environment.db.cluster}.vimj8.mongodb.net/db?retryWrites=true&w=majority`;
const MONGODB_URL =
  "mongodb://127.0.0.1:27017/vusa?readPreference=primary&appname=MongoDB%20Compass%20Community&ssl=false";

export const server = new Server(MONGODB_URL);

server.start();
