import { Model } from "../../config/model";
import { AdditionModel, TAddition } from "./model";

export * from "./model";

class Addition extends Model<TAddition> {
  constructor() {
    super(AdditionModel, "addition");
  }
}

export const addition = new Addition();
