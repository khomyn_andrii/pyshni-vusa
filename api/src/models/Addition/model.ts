import { Schema, Document, model, Model, Types } from "mongoose";
import { AdditionDbObject } from "./types";

export type TAddition = AdditionDbObject & Document;
export type TAdditionModel = Model<TAddition>;

const schema = new Schema<TAddition>(
  {
    title: { type: String, required: true, max: 50 },
    weight: { type: String, required: true, min: 0 },
    price: { type: String, required: true, min: 0 },
    image: { type: String, required: true, max: 200 },
    category: {
      type: Types.ObjectId,
      ref: "AdditionCategory",
      required: true,
    },
  },
  { collection: "addition", timestamps: true, versionKey: false }
);

export const AdditionModel = model<TAddition, TAdditionModel>(
  "Addition",
  schema
);
