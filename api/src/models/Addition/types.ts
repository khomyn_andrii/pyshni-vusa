import { Types } from "mongoose";
import { AdditionCategoryDbObject } from "../AdditionCategory/types";

export type AdditionDbObject = {
  _id: Types.ObjectId;
  title: string;
  weight: string;
  price: string;
  image: string;
  category: AdditionCategoryDbObject["_id"];

  createdAt: Date;
  updatedAt: Date;
};
