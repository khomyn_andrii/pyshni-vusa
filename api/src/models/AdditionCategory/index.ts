import { Model } from "../../config/model";
import { NotFoundError } from "../../utils/errors";
import { AdditionCategoryModel, TAdditionCategory } from "./model";

export * from "./model";

class AdditionCategory extends Model<TAdditionCategory> {
  constructor() {
    super(AdditionCategoryModel, "additionCategory");
  }

  public async getAll() {
    try {
      const data = await this.model
        .find(
          {},
          {},
          {
            sort: { position: 1 },
          }
        )
        .populate("additions");
      return data;
    } catch (e) {
      console.error("NotFoundError", e);
      throw new NotFoundError(this.modelName);
    }
  }
}

export const additionCategory = new AdditionCategory();
