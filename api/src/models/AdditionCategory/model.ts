import { Schema, Document, model, Model } from "mongoose";
import { AdditionCategoryDbObject } from "./types";

export type TAdditionCategory = AdditionCategoryDbObject & Document;
export type TAdditionCategoryModel = Model<TAdditionCategory>;

const schema = new Schema<TAdditionCategory>(
  {
    title: { type: String, required: true, max: 100 },
    position: { type: Number, required: true },
  },
  {
    collection: "additionCategory",
    toJSON: {
      virtuals: true,
    },
    toObject: {
      virtuals: true,
    },
    versionKey: false,
    id: false,
  }
);

schema.virtual("additions", {
  ref: "Addition",
  localField: "_id",
  foreignField: "category",
});

export const AdditionCategoryModel = model<
  TAdditionCategory,
  TAdditionCategoryModel
>("AdditionCategory", schema);
