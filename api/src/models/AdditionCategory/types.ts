import { Types } from "mongoose";
import { AdditionDbObject } from "../Addition/types";

export type AdditionCategoryDbObject = {
  _id: Types.ObjectId;
  title: string;
  position: number;
  additions: AdditionDbObject[];
};
