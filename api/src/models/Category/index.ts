import { Model } from "../../config/model";
import { CategoryModel, TCategory } from "./model";

export * from "./model";

class Category extends Model<TCategory> {
  constructor() {
    super(CategoryModel, "category");
  }
}

export const category = new Category();
