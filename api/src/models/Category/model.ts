import { Schema, Document, model, Model } from "mongoose";
import { CategoryDbObject } from "./types";

export type TCategory = CategoryDbObject & Document;
export type TCategoryModel = Model<TCategory>;

const schema = new Schema<TCategory>(
  {
    title: { type: String, required: true, max: 100 },
    position: { type: Number, required: true },
    showAdditions: { type: Boolean, required: true, default: false },
  },
  { collection: "category", timestamps: true, versionKey: false }
);

export const CategoryModel = model<TCategory, TCategoryModel>(
  "Category",
  schema
);
