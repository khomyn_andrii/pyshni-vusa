import { Types } from "mongoose";

export type CategoryDbObject = {
  _id: Types.ObjectId;
  title: string;
  position: number;

  createdAt: Date;
  updatedAt: Date;
};
