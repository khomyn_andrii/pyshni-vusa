import { product, TProduct } from "./Product";
import { TCategory, category } from "./Category";
import { promotion, TPromotion } from "./Promotion";
import { texts, TTexts } from "./Texts";
import { order, TOrder } from "./Order";
import { additionCategory, TAdditionCategory } from "./AdditionCategory";
import { addition, TAddition } from "./Addition";
import { user, TUser } from "./User";

export const db = {
  product,
  category,
  promotion,
  texts,
  order,
  additionCategory,
  addition,
  user,
};

export type DB = typeof db;

export type TDocuments =
  | TProduct
  | TCategory
  | TPromotion
  | TTexts
  | TOrder
  | TAdditionCategory
  | TAddition
  | TUser;
