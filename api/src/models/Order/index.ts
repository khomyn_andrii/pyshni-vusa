import { Model } from "../../config/model";
import { NotFoundError } from "../../utils/errors";
import { TOrder, OrderModel } from "./model";

export * from "./model";

export class Order extends Model<TOrder> {
  constructor() {
    super(OrderModel, "order");
  }

  public async getCustomers({ skip }: { skip: number }): Promise<TOrder[]> {
    try {
      const data = await this.model.aggregate([
        {
          $group: {
            _id: "$customerPhone",
            phone: { $first: "$customerPhone" },
            email: { $first: "$customerEmail" },
            name: { $first: "$customerName" },
            orderLastDate: { $first: "$createdAt" },
            orderCount: { $sum: 1 },
          },
        },
        {
          $project: {
            _id: 0,
            phone: 1,
            email: 1,
            name: 1,
            orderCount: 1,
            orderLastDate: 1,
          },
        },
        { $sort: { orderLastDate: -1 } },
        { $skip: skip },
        { $limit: 15 },
      ]);

      return data;
    } catch (e) {
      console.error("NotFoundError", e);
      throw new NotFoundError("order");
    }
  }
}

export const order = new Order();
