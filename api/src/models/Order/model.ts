import { Schema, Types, Document, model, Model } from "mongoose";
import { TProductTags } from "../Product/types";
import {
  OrderDbObject,
  OrderStatusEnum,
  OrderPaymentEnum,
  TOrderProduct,
  OrderDeliveryEnum,
} from "./types";

export type TOrder = OrderDbObject & Document;
export type TOrderModel = Model<TOrder>;

const OrderProductAddition = new Schema<TOrderProduct["additions"] & Document>({
  title: { type: String, required: true, max: 50 },
  weight: { type: String, required: true, min: 0 },
  price: { type: String, required: true, min: 0 },
  image: { type: String, required: true, max: 200 },
  quantity: { type: Number, required: true, min: 1 },
});

const OrderProduct = new Schema<TOrderProduct["product"] & Document>({
  product: {
    type: {
      _id: { type: Types.ObjectId, required: true },
      title: { type: String, required: true, max: 100 },
      image: { type: String, required: true, max: 200 },
      description: { type: String, required: true, max: 500 },
      tags: [
        {
          type: String,
          enum: Object.values(TProductTags),
          required: true,
        },
      ],
      createdAt: { type: Date, required: true },
      updatedAt: { type: Date, required: true },
    },
    required: true,
  },
  choosenSize: {
    type: {
      _id: { type: Types.ObjectId, required: true },
      title: { type: String, required: true, max: 50 },
      weight: { type: String, required: true, min: 0 },
      diameter: { type: String, required: true, min: 0 },
      price: { type: String, required: true, min: 0 },
    },
    required: true,
  },
  quantity: { type: Number, required: true, min: 1 },
  additions: {
    type: [{ type: OrderProductAddition, required: true }],
    required: true,
  },
});

const schema = new Schema<TOrder>(
  {
    customerName: { type: String, required: true, max: 100 },
    customerPhone: { type: String, required: true, max: 100 },
    customerEmail: { type: String, required: false, max: 100 },
    telegramMessageId: { type: Number, required: false },
    products: {
      type: [{ type: OrderProduct, required: true }],
      required: true,
      validate: (v: TOrderProduct["product"]) =>
        Array.isArray(v) && v.length > 0,
    },
    comment: { type: String, max: 400 },
    date: { type: Date, required: true },
    changeFrom: { type: String, max: 50 },
    address: { type: String, max: 500, required: false },
    callback: { type: Boolean, required: true },
    status: {
      type: String,
      enum: Object.values(OrderStatusEnum),
      required: true,
    },
    deliveryType: {
      type: String,
      enum: Object.values(OrderDeliveryEnum),
      required: true,
    },
    paymentType: {
      type: String,
      enum: Object.values(OrderPaymentEnum),
      required: true,
    },
    key: {
      type: Number,
      required: true,
      unique: true,
    },
  },
  {
    collection: "order",
    timestamps: true,
    toJSON: { virtuals: true },
    toObject: { virtuals: true },
    versionKey: false,
  }
);

schema.index({ "customer.phone": "text" });

schema.virtual("sum", {}).get(function (this: Omit<TOrder, "sum">) {
  return this.products.reduce((value, item) => {
    const price = isNaN(Number(item.choosenSize.price))
      ? 0
      : Number(item.choosenSize.price);

    const additionsSum = item.additions.reduce(
      (additionValue, additionItem) => {
        const aadditionPrice = isNaN(Number(additionItem.price))
          ? 0
          : Number(additionItem.price);

        return additionValue + additionItem.quantity * aadditionPrice;
      },
      0
    );

    return value + item.quantity * (price + additionsSum);
  }, 0);
});

export const OrderModel = model<TOrder, TOrderModel>("Order", schema);
