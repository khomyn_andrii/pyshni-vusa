import { Types } from "mongoose";
import { AdditionDbObject } from "../Addition/types";
import { ProductDbObject, TProductSize } from "../Product/types";

export type TOrderProduct = {
  product: Omit<
    ProductDbObject,
    "sizes" | "category" | "available" | "updatedAt" | "createdAt"
  >;
  additions: Omit<
    AdditionDbObject & {
      quantity: number;
    },
    "category" | "updatedAt" | "createdAt"
  >[];
  choosenSize: TProductSize;
  quantity: number;
};

export enum OrderPaymentEnum {
  card = "card",
  cash = "cash",
}

export enum OrderStatusEnum {
  new = "new",
  accepted = "accepted",
  canceled = "canceled",
  completed = "completed",
}

export enum OrderDeliveryEnum {
  delivery = "delivery",
  pickup = "pickup",
}

export type OrderDbObject = {
  _id: Types.ObjectId;
  products: TOrderProduct[];
  deliveryType: OrderDeliveryEnum;
  address?: string;
  key: number;
  customerName: string;
  customerPhone: string;
  customerEmail?: string;

  comment?: string;
  changeFrom?: string;
  date: Date;
  paymentType: OrderPaymentEnum;
  status: OrderStatusEnum;
  sum: number;
  callback: boolean;
  telegramMessageId?: number;

  createdAt: Date;
  updatedAt: Date;
};
