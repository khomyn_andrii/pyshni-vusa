import { Model } from "../../config/model";
import { TProduct, ProductModel } from "./model";

export * from "./model";

export class Product extends Model<TProduct> {
  constructor() {
    super(ProductModel, "product");
  }
}

export const product = new Product();
