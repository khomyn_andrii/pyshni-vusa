import { Schema, Types, Document, model, Model } from "mongoose";
import {
  ProductDbObject,
  TProductTags,
  TProductSize,
  WeightTypes,
} from "./types";

export type TProduct = ProductDbObject & Document;
export type TProductModel = Model<TProduct>;

const ProductSizes = new Schema<TProductSize & Document>({
  title: { type: String, required: true, max: 50 },
  weight: { type: String, required: true },
  diameter: { type: String, required: true },
  price: { type: String, required: true },
});

const schema = new Schema<TProduct>(
  {
    title: { type: String, required: true, max: 100 },
    available: { type: Boolean, required: true },
    category: {
      type: Types.ObjectId,
      ref: "Category",
      required: true,
    },
    image: { type: String, required: true, max: 200 },
    description: { type: String, required: true, max: 500 },
    tags: [
      {
        type: String,
        enum: Object.values(TProductTags),
        required: true,
      },
    ],
    weightTypes: {
      type: String,
      enum: Object.values(WeightTypes),
      required: true,
    },
    sizes: {
      type: [{ type: ProductSizes, required: true }],
      required: true,
      validate: (v: TProduct["sizes"]) => Array.isArray(v) && v.length > 0,
    },
  },
  { collection: "product", timestamps: true, versionKey: false }
);

export const ProductModel = model<TProduct, TProductModel>("Product", schema);
