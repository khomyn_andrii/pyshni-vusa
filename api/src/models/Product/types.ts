import { Types } from "mongoose";
import { CategoryDbObject } from "../Category/types";

export type TProductSize = {
  _id: Types.ObjectId;
  title: string;
  weight: string;
  diameter: string;
  price: string;
};

export enum TProductTags {
  spicy = "spicy",
  vegan = "vegan",
  bbq = "bbq",
}

export enum WeightTypes {
  gram = "gram",
  milliliter = "milliliter",
}

export type ProductDbObject = {
  _id: Types.ObjectId;
  title: string;
  description: string;
  image: string;
  available: boolean;
  category: CategoryDbObject["_id"];
  tags: TProductTags[];
  sizes: TProductSize[];
  weightTypes: WeightTypes;

  createdAt: Date;
  updatedAt: Date;
};
