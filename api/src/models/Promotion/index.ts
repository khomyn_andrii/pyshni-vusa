import { Model } from "../../config/model";
import { PromotionModel, TPromotion } from "./model";

export * from "./model";

class Promotion extends Model<TPromotion> {
  constructor() {
    super(PromotionModel, "promotion");
  }
}

export const promotion = new Promotion();
