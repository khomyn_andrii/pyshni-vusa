import { Schema, Document, model, Model } from "mongoose";
import { PromotionDbObject } from "./types";

export type TPromotion = PromotionDbObject & Document;
export type TPromotionModel = Model<TPromotion>;

const schema = new Schema<TPromotion>(
  {
    title: { type: String, required: true, max: 100 },
    description: { type: String, required: true, max: 1000 },
    image: { type: String, required: true, max: 1000 },
    carouselImage: { type: String, required: true, max: 1000 },
    showMain: { type: Boolean, required: true },
    position: { type: Number, required: true },
  },
  { collection: "promotion", timestamps: true, versionKey: false }
);

export const PromotionModel = model<TPromotion, TPromotionModel>(
  "Promotion",
  schema
);
