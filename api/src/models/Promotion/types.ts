import { Types } from "mongoose";

export type PromotionDbObject = {
  _id: Types.ObjectId;
  title: string;
  description: string;
  image: string;
  carouselImage: string;
  showMain: boolean;
  position: number;

  createdAt: Date;
  updatedAt: Date;
};
