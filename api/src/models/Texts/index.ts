import { Model } from "../../config/model";
import { TTexts, TextsModel } from "./model";

export * from "./model";

class Texts extends Model<TTexts> {
  constructor() {
    super(TextsModel, "texts");
  }
}

export const texts = new Texts();
