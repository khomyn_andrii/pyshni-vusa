import { Schema, Document, model, Model } from "mongoose";
import { TextsDbObject } from "./types";

export type TTexts = TextsDbObject & Document;
export type TTextsModel = Model<TTexts>;

const schema = new Schema<TTexts>(
  {
    firstPhone: { type: String, required: false, max: 30 },
    secondPhone: { type: String, required: false, max: 30 },
    thirdPhone: { type: String, required: false, max: 30 },
    facebook: { type: String, required: false, max: 1000 },
    instagram: { type: String, required: false, max: 1000 },
    telegram: { type: String, required: false, max: 1000 },
    startWorkTime: { type: String, required: true, max: 10 },
    endWorkTime: { type: String, required: true, max: 10 },
    seoText: { type: String, required: false },
    contactsText: { type: String, required: false },
    mapLink: {
      type: String,
      required: false,
      max: 1000,
      set: (v: string) => {
        // eslint-disable-next-line no-useless-escape
        const re = /(?:(?:https?):\/\/)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/gim;
        const res = v.match(re);
        if (res && res[0]) {
          return res[0];
        }
      },
    },
    address: { type: String, required: true, max: 100 },
    secondAddress: { type: String, required: false, max: 100 },
    promotionText: { type: String, required: false, max: 100 },
    minOrderTime: { type: Number, required: true, min: 0 },
    deliveryText: { type: String, required: false },
    deliveryImage: { type: String, required: false, max: 1000 },
  },
  { collection: "texts", timestamps: true, versionKey: false }
);

export const TextsModel = model<TTexts, TTextsModel>("Texts", schema);
