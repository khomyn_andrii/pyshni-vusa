import { Types } from "mongoose";

export type TextsDbObject = {
  _id: Types.ObjectId;
  firstPhone: string;
  secondPhone: string;
  thirdPhone: string;
  startWorkTime: string;
  endWorkTime: string;
  address: string;
  secondAddress: string;
  seoText: string;
  facebook: string;
  instagram: string;
  telegram: string;
  contactsText: string;
  mapLink: string;
  promotionText: string;
  minOrderTime: number;
  deliveryImage: string;
  deliveryText: string;
};
