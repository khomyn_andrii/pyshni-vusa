import { Model } from "../../config/model";
import { TUser, UserModel } from "./model";

export * from "./model";

class User extends Model<TUser> {
  constructor() {
    super(UserModel, "user");
  }
}

export const user = new User();
