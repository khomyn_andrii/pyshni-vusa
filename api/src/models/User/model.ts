import { Schema, Document, model, Model } from "mongoose";
import { UserDbObject } from "./types";

export type TUser = UserDbObject & Document;
export type TUserModel = Model<TUser>;

const schema = new Schema<TUser>(
  {
    name: { type: String, required: false, max: 100 },
    password: { type: String, required: false, max: 100 },
  },
  { collection: "user", timestamps: true, versionKey: false }
);

export const UserModel = model<TUser, TUserModel>("user", schema);
