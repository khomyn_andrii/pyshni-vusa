import { Types } from "mongoose";

export type UserDbObject = {
  _id: Types.ObjectId;
  name: string;
  password: string;
};
