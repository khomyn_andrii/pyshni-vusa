import * as express from "express";
import { body, param } from "express-validator";
import { post, patch, destroy } from "../controllers/addition";
import { expressAuth } from "../utils/auth";
import { validate } from "../utils/validate";

const router: express.Router = express.Router();

router.put(
  "/:id",
  expressAuth,
  // param
  param("id").isString(),
  // body
  body("title").isString().optional(),
  body("image").isString().optional(),
  body("category").isString().optional(),
  body("weight").isString().optional(),
  body("price").isString().optional(),
  validate,
  (req, res) => patch(req, res)
);

router.post(
  "/",
  expressAuth,
  body("title").isString(),
  body("image").isString(),
  body("category").isString(),
  body("weight").isString(),
  body("price").isString(),
  validate,
  (req, res) => post(req, res)
);

router.delete(
  "/:id",
  expressAuth,
  param("id").isString(),
  validate,
  (req, res) => destroy(req, res)
);

export default router;
