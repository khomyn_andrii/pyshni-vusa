import * as express from "express";
import { body } from "express-validator";
import { login } from "../controllers/auth";
import { expressAuth } from "../utils/auth";
import { validate } from "../utils/validate";

const router: express.Router = express.Router();

router.post(
  "/login",
  body(["name", "password"]).isString(),
  validate,
  (req, res) => login(req, res)
);

router.post("/token", expressAuth, (_req, res) =>
  res.status(200).send({ tokenStatus: "valid" })
);

export default router;
