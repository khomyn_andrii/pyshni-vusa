import * as express from "express";
import { body, param, query } from "express-validator";
import { all, post, patch, destroy, byId } from "../controllers/category";
import { expressAuth } from "../utils/auth";
import { validate } from "../utils/validate";

const router: express.Router = express.Router();

router.get("/", (req, res) => all(req, res));

router.get("/:id", query("id").isString(), validate, (req, res) =>
  byId(req, res)
);

router.put(
  "/:id",
  expressAuth,
  // param
  param("id").isString(),
  // body
  body("title").isString().optional(),
  body("position").isInt().optional(),
  validate,
  (req, res) => patch(req, res)
);

router.post("/", expressAuth, body("title").isString(), validate, (req, res) =>
  post(req, res)
);

router.delete(
  "/:id",
  expressAuth,
  param("id").isString(),
  validate,
  (req, res) => destroy(req, res)
);

export default router;
