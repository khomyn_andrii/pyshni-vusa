import * as express from "express";
import { query } from "express-validator";
import { all } from "../controllers/customer";
import { expressAuth } from "../utils/auth";
import { validate } from "../utils/validate";

const router: express.Router = express.Router();

router.get(
  "/",
  expressAuth,
  query("skip").isInt().optional(),
  validate,
  (req, res) => all(req, res)
);

export default router;
