import express from "express";
import productRoutes from "./productRoutes";
import categoryRoutes from "./categoryRoutes";
import promotionRoutes from "./promotionRoutes";
import textsRoutes from "./textsRoutes";
import orderRoutes from "./orderRoutes";
import uploadRoutes from "./uploadRoutes";
import additionCategoryRoutes from "./additionCategoryRoutes";
import additionRoutes from "./additionRoutes";
import customerRoutes from "./customerRoutes";
import authRoutes from "./authRoutes";

export const router = express.Router();

router.use("/product", productRoutes);
router.use("/category", categoryRoutes);
router.use("/promotion", promotionRoutes);
router.use("/texts", textsRoutes);
router.use("/order", orderRoutes);
router.use("/upload", uploadRoutes);
router.use("/addition_category", additionCategoryRoutes);
router.use("/addition", additionRoutes);
router.use("/customer", customerRoutes);
router.use("/auth", authRoutes);
