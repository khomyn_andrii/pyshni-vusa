import * as express from "express";
import {
  all,
  post,
  patch,
  destroy,
  byId,
  allLive,
  find,
} from "../controllers/order";
import { body, param, query } from "express-validator";
import { Request, Response } from "express";
import { validate } from "../utils/validate";
import { TProductTags } from "../models/Product/types";
import {
  OrderDeliveryEnum,
  OrderPaymentEnum,
  OrderStatusEnum,
} from "../models/Order/types";
import { expressAuth } from "../utils/auth";

const router: express.Router = express.Router();

router.get(
  "/",
  expressAuth,
  query("skip").isInt().optional(),
  validate,
  (req: Request, res: Response) => all(req, res)
);

router.get("/live", expressAuth, (req: Request, res: Response) =>
  allLive(req, res)
);

router.get(
  "/find",
  expressAuth,
  query("text").isString(),
  validate,
  (req: Request, res: Response) => find(req, res)
);

router.get("/:id", expressAuth, param("id").isString(), validate, (req, res) =>
  byId(req, res)
);

router.put(
  "/:id",
  // param
  param("id").isString(),
  // body
  body("status").isIn(Object.values(OrderStatusEnum)),
  validate,
  (req, res) => {
    patch(req, res);
  }
);

router.post(
  "/",
  // body
  body("products").isArray().notEmpty(),
  body("callback").isBoolean(),
  body([
    "customerName",
    "customerPhone",
    "products.*.product._id",
    "products.*.product.title",
    "products.*.product.image",
    "products.*.product.description",
    "products.*.choosenSize._id",
    "products.*.choosenSize.title",
    "products.*.choosenSize.weight",
    "products.*.choosenSize.diameter",
    "products.*.choosenSize.price",
    "products.*.additions.*.title",
    "products.*.additions.*.image",
  ]).isString(),
  body(["comment", "changeFrom", "customerEmail"]).isString().optional(),
  body(["date"]).isISO8601(),
  body("products.*.product.tags")
    .isArray()
    .custom((r) => {
      return r.every((v: TProductTags) =>
        Object.values(TProductTags).includes(v)
      );
    }),
  body("paymentType").isIn(Object.values(OrderPaymentEnum)),
  body("status").isIn(Object.values(OrderStatusEnum)),
  body("deliveryType").isIn(Object.values(OrderDeliveryEnum)),
  body("address").isString().optional(),
  body(["products.*.quantity", "products.*.additions.*.quantity"]).isInt(),
  body([
    "products.*.additions.*.weight",
    "products.*.additions.*.price",
  ]).isFloat(),
  validate,
  (req, res) => post(req, res)
);

router.delete(
  "/:id",
  expressAuth,
  param("id").isString(),
  validate,
  (req, res) => destroy(req, res)
);

export default router;
