import * as express from "express";
import { all, post, patch, destroy, byId } from "../controllers/products";
import { body, param, query } from "express-validator";
import { Request, Response } from "express";
import { validate } from "../utils/validate";
import { TProductTags, WeightTypes } from "../models/Product/types";
import { expressAuth } from "../utils/auth";

const router: express.Router = express.Router();

router.get(
  "/",
  // param
  query("category").isString().optional(),
  validate,
  (req: Request<{}, any, {}, { category: string }>, res: Response) =>
    all(req, res)
);

router.get("/:id", query("id").isString(), validate, (req, res) =>
  byId(req, res)
);

router.put(
  "/:id",
  expressAuth,
  // param
  param("id").isString(),
  // body
  body(["title", "description", "image", "category"]).isString().optional(),
  body("available").isBoolean().optional(),
  body("tags")
    .isArray()
    .custom((r) => {
      return r.every((v: TProductTags) =>
        Object.values(TProductTags).includes(v)
      );
    })
    .optional(),
  body("weightTypes").isIn(Object.values(WeightTypes)).optional(),
  body("sizes").isArray().notEmpty().optional(),
  body("sizes.*.title").isString().optional(),
  body(["sizes.*.weight", "sizes.*.diameter", "sizes.*.price"])
    .isString()
    .optional(),
  validate,
  (req, res) => {
    patch(req, res);
  }
);

router.post(
  "/",
  expressAuth,
  // body
  body(["title", "description", "image", "category"]).isString(),
  body("available").isBoolean(),
  body("tags")
    .isArray()
    .custom((r) => {
      return r.every((v: TProductTags) =>
        Object.values(TProductTags).includes(v)
      );
    }),
  body("weightTypes").isIn(Object.values(WeightTypes)),
  body("sizes").isArray().notEmpty(),
  body("sizes.*.title").isString(),
  body(["sizes.*.weight", "sizes.*.diameter", "sizes.*.price"]).isString(),
  validate,
  (req, res) => post(req, res)
);

router.delete(
  "/:id",
  expressAuth,
  param("id").isString(),
  validate,
  (req, res) => destroy(req, res)
);

export default router;
