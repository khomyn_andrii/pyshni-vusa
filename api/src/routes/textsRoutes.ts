import * as express from "express";
import { body } from "express-validator";
import { get, patch } from "../controllers/texts";
import { expressAuth } from "../utils/auth";
import { validate } from "../utils/validate";

const router: express.Router = express.Router();

router.get("/", (req, res) => get(req, res));

router.put(
  "/",
  expressAuth,
  // body
  body([
    "firstPhone",
    "secondPhone",
    "thirdPhone",
    "facebook",
    "instagram",
    "telegram",
    "startWorkTime",
    "endWorkTime",
    "seoText",
    "contactsText",
    "mapLink",
    "address",
    "secondAddress",
    "promotionText",
    "deliveryText",
    "deliveryImage",
  ])
    .isString()
    .optional(),
  body("minOrderTime").isInt().optional(),
  validate,
  (req, res) => patch(req, res)
);

export default router;
