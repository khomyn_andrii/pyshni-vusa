import express, { Response } from "express";
import { expressAuth } from "../utils/auth";
import { uploadImageMulter, removeAllImages, removeDir } from "../utils/upload";

const router = express.Router();

router.post(
  "/image",
  expressAuth,
  uploadImageMulter.single("file"),
  async (req, res) => writeImages(req, res)
);

const writeImages = async (req: any, res: Response) => {
  try {
    const { path } = req.file;

    if (req.body.delete) {
      await removeAllImages(req.body.delete);
      await removeDir(req.body.delete);
    }

    res.status(200).send(JSON.stringify({ path }));
  } catch (error) {
    console.log(error);
    res.status(500).send(JSON.stringify({ error }));
  }
};

export default router;
