import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import jwtExpress from "express-jwt";
import { environment } from "../environment";

export const generateToken = (id: string) => {
  const jwtPayload = { id };
  const jwtData = { expiresIn: environment.jwt.duration };

  const token = jwt.sign(jwtPayload, environment.jwt.secret, jwtData);
  return token;
};

export const createHash = (text: string) => {
  const hash = bcrypt.hashSync(text, 11);
  return hash;
};

export const expressAuth = jwtExpress({
  secret: environment.jwt.secret,
  algorithms: ["HS256"],
});
