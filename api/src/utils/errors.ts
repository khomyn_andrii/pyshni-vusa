/* eslint-disable max-classes-per-file */
import { DB } from "../models";

class AuthError extends Error {
  public status: number;

  constructor() {
    super();
    this.name = "AuthError";
    this.status = 401;
  }
}

class NotFoundError extends Error {
  public where: keyof DB;
  public status: number;

  constructor(where: keyof DB) {
    super();
    this.name = "NotFoundError";
    this.where = where;
    this.status = 404;
  }
}

class CreateError extends Error {
  public where: keyof DB;
  public status: number;

  constructor(where: keyof DB) {
    super();
    this.name = "CreateError";
    this.where = where;
    this.status = 500;
  }
}

class UpdateError extends Error {
  public status: number;
  public where: keyof DB;

  constructor(where: keyof DB) {
    super();
    this.name = "UpdateError";
    this.where = where;
    this.status = 500;
  }
}

class DeleteError extends Error {
  public status: number;
  public where: keyof DB;

  constructor(where: keyof DB) {
    super();
    this.name = "DeleteError";
    this.where = where;
    this.status = 500;
  }
}

export { NotFoundError, AuthError, CreateError, UpdateError, DeleteError };
