import { Request, Express } from "express";
import multer from "multer";
import fs, { promises } from "fs";
import { v4 as uuidv4 } from "uuid";
import path from "path";

const multerFilter = (
  req: Request<any>,
  file: Express.Multer.File,
  cb: multer.FileFilterCallback
) => {
  if (file.mimetype.startsWith("image")) {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const dirDestination = (table: string) => {
  const random = Math.random() * 100000;
  const dir = `uploads/${table}/${Math.round(random)}`;
  fs.mkdirSync(dir, { recursive: true });
  return dir;
};

const multerStorage = (dir: string) =>
  multer.diskStorage({
    destination(req, file, cb) {
      cb(null, dirDestination(dir));
    },
    filename(req, file, cb) {
      cb(null, `${uuidv4()}.jpg`);
    },
  });

const createMulter = (name: string) => {
  return multer({
    storage: multerStorage(name),
    fileFilter: multerFilter,
  });
};

export const uploadImageMulter = createMulter("image");

export const removeFile = async (localPath: string) => {
  try {
    await promises.unlink(localPath);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
};

export const removeAllImages = async (path: string) => {
  const pathArray = path.split(".");
  const image = removeFile(path);
  const small = removeFile(`${pathArray[0]}_small.${pathArray[1]}`);
  const medium = removeFile(`${pathArray[0]}_medium.${pathArray[1]}`);
  const large = removeFile(`${pathArray[0]}_large.${pathArray[1]}`);
  await Promise.all([image, small, medium, large]);
};

export const removeDir = async (localPath: string) => {
  try {
    const dir = path.dirname(localPath);
    await promises.rmdir(dir);
  } catch (e) {
    // eslint-disable-next-line no-console
    console.log(e);
  }
};
