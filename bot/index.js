process.env.NTBA_FIX_319 = 1;

const TelegramBot = require("node-telegram-bot-api");
const fetch = require("node-fetch");

const token = "1793130388:AAH-tZ54W2TwU43M1XyB55CVjI6O8EypnQQ";

const bot = new TelegramBot(token, { polling: true });

bot.on("callback_query", async (query) => {
  try {
    const data = JSON.parse(query.data);
    if (data && data.id && data.type) {
      await fetch(`http://77.87.197.181:8080/order/${data.id}`, {
        method: "PUT",
        body: JSON.stringify({
          status: data.type,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });
      await bot.answerCallbackQuery(query.id, {
        text: "Обновление статуса заказа прошло успешно!",
      });
    } else {
      throw new Error();
    }
  } catch (e) {
    bot.answerCallbackQuery(query.id, {
      text: "Произошла ошибка, попробуйте ещё раз!",
    });
  }
});
