import React from 'react'
import { RenameHeader } from '../Framework'
import add from '../img/add.svg'
import { IAddition, IAdditionCategory } from '../../utils/types'
import {
  removeAdditionCategory,
  addItemToCategory,
} from '../../store/additionCategory'
import { AdditionItem } from './AdditionItem'
import { useAppDispatch } from '../../store/hooks'

interface Props {
  category: IAdditionCategory
}

export const AdditionCategory = ({ category }: Props) => {
  const dispatch = useAppDispatch()

  let key = 0

  const emptyAddition = {
    title: 'Пустое дополнение',
    weight: 0,
    price: 0,
    image: 'empty',
    category: category._id,
  } as IAddition

  return (
    <>
      <RenameHeader
        route="/addition_category"
        item={category}
        removeAction={removeAdditionCategory}
      />
      <div className="admin__additions__content">
        {category.additions.map((addition) => (
          <AdditionItem
            addition={addition}
            categoryId={category._id}
            key={addition._id || key++}
          />
        ))}
        <div className="admin__additions__add">
          <div
            className="admin__additions__add__btn"
            onClick={() => {
              let id = category.additions.length + 1
              dispatch(
                addItemToCategory({
                  categoryId: category._id,
                  addition: { ...emptyAddition, _id: id.toString() },
                })
              )
            }}
          >
            <img src={add} alt="" />
          </div>
        </div>
      </div>
    </>
  )
}
