import React, { useState } from 'react'
import { IAddition } from '../../utils/types'
import trash from '../img/trash.svg'
import edit from '../img/edit.svg'
import save from '../img/save.svg'
import AxiosService from '../../utils/axiosService'
import { useAppDispatch } from '../../store/hooks'
import {
  removeItemFromCategory,
  updateItemInCategory,
} from '../../store/additionCategory'
import { setError } from '../../store/error'
import noImage from '../../utils/noImage'

interface Props {
  addition: IAddition
  categoryId: string
}

export const AdditionItem = ({ addition, categoryId }: Props) => {
  const dispatch = useAppDispatch()
  const isNewItem = Number.isInteger(+addition._id)
  const [editMode, setEditMode] = useState(isNewItem)
  const [confirmDelete, setConfirmDelete] = useState(false)
  const [disabled, setDisabled] = useState(false)

  const [image, setImage] = useState(addition.image)
  const [editedTitle, setEditedTitle] = useState(addition.title)
  const [editedWeight, setEditedWeight] = useState(
    addition.weight.toString() || '0'
  )
  const [editedPrice, setEditedPrice] = useState(
    addition.price.toString() || '0'
  )

  const service = new AxiosService()
  const fileInput = React.createRef<any>()

  const handleSave = async () => {
    if (disabled) {
      return
    }

    if (
      !editedTitle ||
      !editedWeight ||
      !editedPrice ||
      editedWeight === 'NaN' ||
      editedPrice === 'NaN'
    ) {
      return dispatch(setError('Поля дополнения не должно быть пустыми'))
    }

    setDisabled(true)

    let path = null
    if (fileInput && fileInput.current && fileInput.current.files[0]) {
      const imageFile = fileInput.current.files[0]
      const data = new FormData()
      data.append('file', imageFile)
      const imageUrl = await service.post('/upload/image', data)
      path = process.env.REACT_APP_SERVER + '/' + imageUrl.path
    }

    const newAddition = {
      title: editedTitle,
      weight: editedWeight,
      price: editedPrice,
      category: categoryId,
      image: path,
    }

    if (isNewItem) {
      if (path === null) {
        setDisabled(false)
        return dispatch(setError('Фотография дополнения не должна быть пустой'))
      }
      const res = await service.post('/addition', newAddition, true)

      dispatch(
        updateItemInCategory({
          categoryId,
          addition: res,
          idToUpdate: addition._id,
        })
      )
    } else {
      const res = await service.put(
        '/addition/' + addition._id,
        {
          ...newAddition,
          image: path ? path : addition.image,
        },
        true
      )
      dispatch(
        updateItemInCategory({
          categoryId,
          addition: res,
          idToUpdate: addition._id,
        })
      )
    }
    setDisabled(false)
    setEditMode(false)
  }

  const renderActions = () => {
    if (confirmDelete) {
      return (
        <>
          <button
            className="confirm"
            onClick={async () => {
              const res = await service.delete('/addition/' + addition._id)
              res && dispatch(removeItemFromCategory({ categoryId, addition }))
            }}
          >
            ✓
          </button>
          <button className="cancel" onClick={() => setConfirmDelete(false)}>
            ✕
          </button>
        </>
      )
    }
    if (editMode) {
      return (
        <>
          <div
            className={'icon' + (disabled ? ' disabled' : '')}
            onClick={handleSave}
          >
            <img src={save} alt="" />
          </div>
        </>
      )
    }

    return (
      <>
        <div className="icon">
          <img src={trash} alt="" onClick={() => setConfirmDelete(true)} />
        </div>
        <div className="icon">
          <img src={edit} alt="" onClick={() => setEditMode(true)} />
        </div>
      </>
    )
  }

  return (
    <div className="admin__additions__row" key={addition._id}>
      <div
        className={'admin__additions__row__img' + (editMode ? ' editable' : '')}
        style={{ backgroundImage: `url(${image}), url(${noImage})` }}
      >
        <label className="admin__additions__file">
          <img src={edit} alt="" />
          <input
            type="file"
            ref={fileInput}
            accept="image/*"
            onChange={() => {
              const param = fileInput.current.files[0]
              const res = URL.createObjectURL(param)
              setImage(res)
            }}
          />
        </label>
      </div>
      <span className="admin__additions__row__header">Название</span>
      <span className="admin__additions__row__header">Вес (в грамах)</span>
      <span className="admin__additions__row__header">Цена (в грн)</span>
      {!editMode ? (
        <>
          <span className="admin__additions__row__item">{addition.title}</span>
          <span className="admin__additions__row__item">{addition.weight}</span>
          <span className="admin__additions__row__item">{addition.price}</span>
        </>
      ) : (
        <>
          <input
            type="text"
            className="admin__additions__row__input"
            value={editedTitle}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setEditedTitle(e.target.value)
            }
          />
          <input
            type="number"
            className="admin__additions__row__input"
            value={parseFloat(editedWeight)}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setEditedWeight(parseFloat(e.target.value).toString())
            }
          />
          <input
            type="number"
            className="admin__additions__row__input"
            value={parseFloat(editedPrice)}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setEditedPrice(parseFloat(e.target.value).toString())
            }
          />
        </>
      )}
      <span className="admin__additions__row__actions">{renderActions()}</span>
    </div>
  )
}
