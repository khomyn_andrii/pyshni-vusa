import React from 'react'
import { addAdditionCategory } from '../../store/additionCategory'
import { useAppSelector } from '../../store/hooks'
import { AddItemHeader } from '../Framework'
import { AdditionCategory } from './AdditionCategory'

import './Additions.scss'

export const Additions = () => {
  const { additionCategories } = useAppSelector(
    (state) => state.additionCategories
  )

  if (!additionCategories) {
    return null
  }

  return (
    <div className="admin__additions">
      <AddItemHeader
        placeholder="Новое дополнение"
        btnText="Добавить дополнение"
        route="/addition_category"
        addAction={addAdditionCategory}
      />
      {additionCategories.map((categ) => {
        return <AdditionCategory category={categ} key={categ._id} />
      })}
    </div>
  )
}
