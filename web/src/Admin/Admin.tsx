/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { Redirect, Route, useHistory } from 'react-router-dom'
import { Burger } from './AdminBurger'
import { SimpleNav } from './SimpleNav'
import { Menu } from './Menu'
import { TextSection } from './TextSection'
import { EditModal } from './EditModal'
import { useAppSelector } from '../store/hooks'
import { Additions } from './Additions'
import './Admin.scss'
import { LiveOrders } from './LiveOrders'
import { Archive } from './Archive'
import { Customers } from './Customers'
import { PromoSection } from './PromoSection'
import { LoginPage } from './LoginPage'
import AxiosService from '../utils/axiosService'
import { ITexts } from '../utils/types'

interface RoutesProps {
  texts: ITexts
}

const AllRoutes = ({ texts }: RoutesProps) => {
  return (
    <>
      <Route
        exact
        path="/admin/"
        render={() => <Redirect to="/admin/orders" />}
      />
      <Route path="/admin/text" render={() => <TextSection texts={texts} />} />
      <Route path="/admin/archive" component={Archive} />
      <Route path="/admin/orders" component={LiveOrders} />
      <Route path="/admin/menu" component={Menu} />
      <Route path="/admin/additions" component={Additions} />
      <Route path="/admin/customers" component={Customers} />
      <Route path="/admin/promotions" component={PromoSection} />
    </>
  )
}

export const Admin = () => {
  const service = new AxiosService()
  const history = useHistory()
  const [isOpen, setIsOpen] = useState(false)
  const { isOpen: modalOpen } = useAppSelector((state) => state.modal)
  const { texts } = useAppSelector((state) => state.texts)

  const [isAuth, setIsAuth] = useState(false)

  const fetchLogin = async () => {
    try {
      const res = await service.post('/auth/token', {}, true)
      if (res && res.tokenStatus === 'valid') {
        setIsAuth(true)
      } else {
        history.push('/admin/login')
      }
    } catch (err) {
      history.push('/admin/login')
    }
  }

  useEffect(() => {
    fetchLogin()
  }, [])

  return (
    <div className="admin">
      <SimpleNav setIsOpen={setIsOpen} isOpen={isOpen} />
      {isAuth && <Burger isOpen={isOpen} />}
      <EditModal />
      <div className={'admin__overlay' + (modalOpen ? ' modalOpen' : '')}>
        <div className={'admin__content'}>
          <AllRoutes texts={texts} />
          <Route
            exact
            path="/admin/login"
            render={() => <LoginPage setIsAuth={setIsAuth} />}
          />
        </div>
      </div>
    </div>
  )
}
