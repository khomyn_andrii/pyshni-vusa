import React from 'react'
import { NavLink } from 'react-router-dom'
import archive from '../img/archive.svg'
import menu from '../img/menu.svg'
import orders from '../img/orders.svg'
import text from '../img/text.svg'
import additions from '../img/additions.svg'
import percent from '../img/percent.svg'
import customers from '../img/customers.svg'
import './AdminBurger.scss'

interface Props {
  isOpen: boolean
}

export const Burger = ({ isOpen }: Props) => {
  return (
    <div className={'admin-burger' + (isOpen ? ' open' : '')}>
      <div className="admin-burger__list">
        <NavLink to="/admin/orders">
          <img src={orders} alt="" />
          Live заказы
        </NavLink>
        <NavLink to="/admin/archive">
          <img src={archive} alt="" />
          Архив заказов
        </NavLink>
        <NavLink to="/admin/menu">
          <img src={menu} alt="" />
          Меню
        </NavLink>
        <NavLink to="/admin/additions">
          <img src={additions} alt="" />
          Дополнения
        </NavLink>
        <NavLink to="/admin/customers">
          <img src={customers} alt="" />
          Клиенты
        </NavLink>
        <NavLink to="/admin/promotions">
          <img src={percent} alt="" />
          Акции
        </NavLink>
        <NavLink to="/admin/text">
          <img src={text} alt="" />
          Настройки
        </NavLink>
      </div>
    </div>
  )
}
