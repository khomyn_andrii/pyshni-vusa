import React, { useState } from 'react'
import { useAppDispatch } from '../../store/hooks'
import { openModal } from '../../store/modal'
import noImage from '../../utils/noImage'
import { IProduct } from '../../utils/types'
import './AdminCard.scss'

interface Props {
  product: IProduct
}

export const AdminCard = ({ product }: Props) => {
  const dispatch = useAppDispatch()
  const [active, setActive] = useState<number>(0)
  const [togglePos, setTogglePos] = useState(0)

  if (!product) {
    return null
  }

  const sizesCopy = product.sizes.slice()
  const sizes = sizesCopy.sort((a, b) => a.diameter - b.diameter)
  const fraction = 100 / sizes.length
  let start = 0 - fraction
  const percents = sizes.map(() => Math.round((start += fraction)))

  return (
    <div className={'admin-card' + (!product.available ? ' disabled' : '')}>
      <div
        className="admin-card__image"
        style={{ backgroundImage: `url(${product.image}), url(${noImage})` }}
      ></div>
      <div className="admin-card__weight">
        {Number(sizes[active].weight)}{' '}
        {product.weightTypes === 'gram' ? 'гр' : 'мл'}
      </div>
      <div className="admin-card__title">{product.title}</div>
      <div className="admin-card__description">{product.description}</div>
      <div className="admin-card__size-choose">
        {sizes.map((size, i) => (
          <div
            className={
              'admin-card__size-choose__item' + (active === i ? ' active' : '')
            }
            style={{ width: fraction + '%' }}
            key={i}
            onClick={() => {
              setTogglePos(percents[i])
              setActive(i)
            }}
          >
            <p>
              {Number(size.diameter)} - {size.title}
            </p>
          </div>
        ))}
        <div
          className="admin-card__size-toggle"
          style={{ left: togglePos + '%', width: fraction + '%' }}
        ></div>
      </div>
      <div className="admin-card__bottom">
        <div className="admin-card__bottom__price">
          {Number(sizes[active].price)} <span>грн</span>
        </div>
        <button
          className="admin-card__bottom__edit-btn"
          onClick={() => dispatch(openModal(product))}
        >
          Изменить
        </button>
      </div>
    </div>
  )
}
