/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import AxiosService from '../../utils/axiosService'
import { IOrder } from '../../utils/types'
import { ArchiveCard } from './ArchiveCard'
import searchIcon from '../img/search.svg'
import trashIcon from '../img/trashBlue.svg'
import './Archive.scss'
import { useAppDispatch } from '../../store/hooks'
import { endLoad, startLoad } from '../../store/loading'

export const Archive = () => {
  const dispatch = useAppDispatch()
  const service = new AxiosService()
  const [orders, setOrders] = useState<IOrder[]>([])
  const [searchOrders, setSearchOrders] = useState<IOrder[]>([])
  const [search, setSearch] = useState('')
  const [searchMode, setSearchMode] = useState(false)

  const fetchData = async () => {
    const res = await service.get('/order', {})
    res && setOrders(res.sort((a: any, b: any) => a.key - b.key))
  }

  useEffect(() => {
    fetchData()
  }, [])

  return (
    <div className="archive">
      <div className="archive__search">
        <input
          value={search}
          placeholder="Номер заказа или номер телефона"
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setSearch(e.target.value)
          }}
        />
        <img
          src={searchIcon}
          alt=""
          onClick={async () => {
            dispatch(startLoad())
            const res = await service.get('/order/find?text=' + search, {})
            setSearchOrders(res)
            setSearchMode(true)
            dispatch(endLoad())
          }}
        />
        <img
          src={trashIcon}
          alt=""
          onClick={() => {
            setSearchMode(false)
            setSearch('')
          }}
        />
      </div>
      {searchMode
        ? searchOrders.map((order) => (
            <ArchiveCard order={order} key={order.key} />
          ))
        : orders.map((order) => <ArchiveCard order={order} key={order.key} />)}
      {!searchMode && (
        <div className="archive__more-btn">
          <button
            onClick={async () => {
              dispatch(startLoad())
              const res = await service.get('/order?skip=' + orders.length, {})
              setOrders(orders.concat(res))
              dispatch(endLoad())
            }}
          >
            Еще
          </button>
        </div>
      )}
    </div>
  )
}
