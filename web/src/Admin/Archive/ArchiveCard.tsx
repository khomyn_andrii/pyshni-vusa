import React from 'react'
import { capitalize } from 'lodash'
import moment from 'moment'
import { IOrder } from '../../utils/types'
import { calculateItemPrice } from '../../utils/calculateItemPrice'
import { calculateCartPrice } from '../../utils/calculateCartPrice'

interface Props {
  order: IOrder
}

export const ArchiveCard = ({ order }: Props) => {
  const statuses: { [key: string]: string } = {
    new: 'новый',
    accepted: 'принят',
    canceled: 'отменен',
    completed: 'завершен',
  }

  return (
    <div className="archive__card">
      <div className="archive__header">
        <span>
          <strong>Заказ #{order.key}</strong>
        </span>
        <span>
          Статус: <strong>{statuses[order.status]}</strong>
        </span>
      </div>
      <div className="archive__grid">
        <div className="archive__grid__part">
          <span>
            <strong>Доставка:</strong>{' '}
            {order.deliveryType === 'delivery' ? 'курьером' : 'самовывоз'}
          </span>
          {order.deliveryType !== 'pickup' && (
            <span>
              <strong>Адрес доставки:</strong> {order.address}
            </span>
          )}
          <span>
            <strong>Время:</strong>{' '}
            {capitalize(moment(order.date).format('dddd DD/MM HH:mm'))}
          </span>
          <span>
            <strong>Оплата:</strong>{' '}
            {order.paymentType === 'card' ? 'картой' : 'наличка'}
          </span>
          {!!order.changeFrom && (
            <span>
              <strong>Сдача с:</strong> {order.changeFrom}
            </span>
          )}
          <span>
            <strong>Имя:</strong> {order.customerName}
          </span>
          <span>
            <strong>Телефон:</strong> {order.customerPhone}
          </span>
          <span>
            <strong>E-mail:</strong> {order.customerEmail}
          </span>
          <span>
            <strong>Время создание заказа:</strong>{' '}
            {capitalize(moment(order.createdAt).format('dddd DD/MM HH:mm'))}
          </span>
          <span>
            <strong>Комментарий:</strong> {order.comment}
          </span>
        </div>
        <div className="archive__grid__part">
          {order.products.map((item) => (
            <div
              className="archive__product"
              key={item.product._id + item.quantity + item.choosenSize._id}
            >
              <div className="archive__product__title">
                {item.quantity}х {item.product.title} <br />(
                {item.choosenSize.title} - {item.choosenSize.diameter} см)
              </div>
              {item.additions.length > 0 && (
                <div className="archive__product__additions">
                  Дополнительно:
                  {item.additions.map((a) => (
                    <div key={a._id}>
                      {a.title} x{a.quantity}
                    </div>
                  ))}
                </div>
              )}
              <div className="archive__product__price">
                {calculateItemPrice(item)} грн
              </div>
            </div>
          ))}

          <div className="archive__total-price">
            Итого: {calculateCartPrice(order)} грн
          </div>
        </div>
      </div>
    </div>
  )
}
