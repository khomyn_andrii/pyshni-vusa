/* eslint-disable react-hooks/exhaustive-deps */
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import AxiosService from '../../utils/axiosService'
import { ICustomer } from '../../utils/types'
import './Customers.scss'

export const Customers = () => {
  const service = new AxiosService()
  const [customers, setCustomers] = useState<ICustomer[]>()

  const fetchData = async () => {
    const res = await service.get('/customer', {})
    setCustomers(res)
  }

  useEffect(() => {
    fetchData()
  }, [])

  if (!customers) {
    return null
  }

  return (
    <div className="customers">
      {customers.map((customer) => (
        <div className="customers__item" key={customer.phone + customer.name}>
          <div className="customers__field">
            <span className="customers__field__label">Имя:</span>
            <span className="customers__field__data">{customer.name}</span>
          </div>
          <div className="customers__field">
            <span className="customers__field__label">Номер телефона:</span>
            <span className="customers__field__data">{customer.phone}</span>
          </div>
          <div className="customers__field">
            <span className="customers__field__label">Email:</span>
            <span className="customers__field__data">{customer.email}</span>
          </div>
          <div className="customers__field">
            <span className="customers__field__label">
              Дата последнего заказа:
            </span>
            <span className="customers__field__data">
              {moment(customer.orderLastDate).format('DD/MM/YYYY HH:mm')}
            </span>
          </div>
          <div className="customers__field">
            <span className="customers__field__label">Количество заказов:</span>
            <span className="customers__field__data">
              {customer.orderCount}
            </span>
          </div>
        </div>
      ))}
    </div>
  )
}
