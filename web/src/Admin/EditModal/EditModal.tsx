/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import trash from '../img/trash.svg'
import add from '../img/add.svg'
import './EditModal.scss'
import { closeModal } from '../../store/modal'
import axiosService from '../../utils/axiosService'
import {
  addProductToState,
  removeProductFromState,
  updateProducts,
} from '../../store/product'
import { ISize, WeightTypes } from '../../utils/types'
import { setError } from '../../store/error'
import noImage from '../../utils/noImage'

interface Size {
  title: string
  weight: number
  diameter: number
  price: number
  [key: string]: string | number
}

export const EditModal = () => {
  const dispatch = useAppDispatch()
  const { isOpen, product, category } = useAppSelector((state) => state.modal)
  const [title, setTitle] = useState('')
  const [description, setDescription] = useState('')
  const [image, setImage] = useState('')
  const [available, setAvailable] = useState(true)
  const [confirmDelete, setConfirmDelete] = useState(false)
  const [weightType, setWeightType] = useState<WeightTypes>(WeightTypes.gram)
  const fileInput = React.createRef<any>()

  const emptySize = { title: '', weight: '0', diameter: '0', price: '0' } as any
  const initState: Size[] = [emptySize]
  const [sizes, setSizes] = useState<Size[]>(product?.sizes || initState)

  const service = new axiosService()

  useEffect(() => {
    if (product) {
      setTitle(product.title)
      setDescription(product.description)
      setAvailable(product.available)
      setSizes(product.sizes)
      setImage(product.image)
    }
    return () => {
      setTitle('')
      setDescription('')
      setAvailable(false)
      setSizes(initState)
      setImage('')
    }
  }, [product, category])

  const onInputChange = (field: string, isNumber: boolean, i: number) => {
    return (e: any) => {
      let sizesCopy = [...sizes]
      let item = { ...sizesCopy[i] }
      item[field] = isNumber
        ? parseFloat(e.target.value).toString()
        : e.target.value

      sizesCopy[i] = item
      setSizes(sizesCopy)
    }
  }

  const handleSubmit = async () => {
    if (title === '') {
      return dispatch(setError('Название не должно быть пустым'))
    }
    if (description === '') {
      return dispatch(setError('Описание не должно быть пустым'))
    }
    if (sizes.length === 0) {
      return dispatch(setError('Продукт не должен быть без размеров'))
    }
    if (sizes.map((size) => size.title).some((title) => title === '')) {
      return dispatch(setError('Название размера не должно быть пустым'))
    }

    if (product) {
      let path = null
      if (fileInput && fileInput.current && fileInput.current.files[0]) {
        const imageFile = fileInput.current.files[0]
        const data = new FormData()
        data.append('file', imageFile)
        const imageUrl = await service.post('/upload/image', data)
        path = process.env.REACT_APP_SERVER + '/' + imageUrl.path
      }
      const newProduct = {
        title,
        description,
        available,
        sizes: sizes as ISize[],
        _id: product._id,
        tags: product.tags,
        category: product.category,
        image: path ? path : product.image,
        weightTypes: weightType,
      }
      dispatch(updateProducts(newProduct))
      await service.put('/product/' + product._id, newProduct, true)
    }

    if (category) {
      const imageFile = fileInput.current.files[0]
      if (!imageFile) {
        return dispatch(setError('Фото не должно быть пустым'))
      }
      const data = new FormData()
      data.append('file', imageFile)
      const imageUrl = await service.post('/upload/image', data)
      const path = process.env.REACT_APP_SERVER + '/' + imageUrl.path

      const newProduct = {
        title,
        description,
        available,
        image: path,
        category,
        weightTypes: weightType,
        sizes: sizes as ISize[],
        tags: [],
      }
      try {
        const res = await service.post('/product/', newProduct, true)
        dispatch(addProductToState(res))
      } catch (error) {
        return
      }
    }
    if (fileInput && fileInput.current) {
      fileInput.current.value = ''
    }
    return dispatch(closeModal())
  }

  return (
    <div className={'edit-modal' + (isOpen ? ' open' : '')}>
      <div className="edit-modal__content">
        <div
          className="edit-modal__close-btn"
          onClick={() => {
            dispatch(closeModal())
          }}
        >
          <span></span>
          <span></span>
        </div>
        <div
          className="edit-modal__image"
          style={{ backgroundImage: `url(${image}), url(${noImage})` }}
        ></div>
        <label className="edit-modal__file">
          Выбрать фото
          <input
            type="file"
            ref={fileInput}
            accept="image/*"
            onChange={() => {
              const param = fileInput.current.files[0]
              const res = URL.createObjectURL(param)
              setImage(res)
            }}
          />
        </label>
        <div className="edit-modal__input-title">
          <input
            type="text"
            placeholder="Название"
            value={title}
            onChange={(e: any) => setTitle(e.target.value)}
          />
        </div>
        <textarea
          className="edit-modal__textarea"
          rows={5}
          placeholder="Ингредиенты"
          value={description}
          onChange={(e: any) => setDescription(e.target.value)}
        />
        <label className="edit-modal__checkbox">
          <input
            type="checkbox"
            checked={available}
            onChange={(e: any) => setAvailable(e.target.checked)}
          />
          Есть в наличии
        </label>
        <div className="edit-modal__header">Размеры</div>
        <div className="edit-modal__weight">
          Выберите систему измерения:
          <select
            onChange={(e: React.ChangeEvent<HTMLSelectElement>) =>
              setWeightType(
                WeightTypes[e.target.value as keyof typeof WeightTypes]
              )
            }
          >
            <option value={WeightTypes.gram}> гр </option>
            <option value={WeightTypes.milliliter}>мл</option>
          </select>
        </div>
        <div className="edit-modal__sizes-grid">
          <div className="edit-modal__sizes-grid__header">Название размера</div>
          <div className="edit-modal__sizes-grid__header">Диаметр (см)</div>
          <div className="edit-modal__sizes-grid__header">
            Вес ({weightType === WeightTypes.gram ? 'гр' : 'мл'})
          </div>
          <div className="edit-modal__sizes-grid__header">Цена (грн)</div>
          {sizes.map((size, i) => (
            <div className="edit-modal__sizes-grid__size" key={i}>
              <input
                type="text"
                value={size.title}
                onChange={onInputChange('title', false, i)}
              />
              <input
                type="number"
                value={size.diameter.toString()}
                onChange={onInputChange('diameter', true, i)}
              />
              <input
                type="number"
                value={size.weight.toString()}
                onChange={onInputChange('weight', true, i)}
              />
              <input
                type="number"
                value={size.price.toString()}
                onChange={onInputChange('price', true, i)}
              />
              <div
                className="edit-modal__sizes-grid__size__img"
                onClick={() => setSizes(sizes.filter((_, idx) => idx !== i))}
              >
                <img src={trash} alt="" />
              </div>
            </div>
          ))}
        </div>
        <div className="edit-modal__add-btn">
          <div
            className="edit-modal__add-btn__img"
            onClick={() => setSizes([...sizes, emptySize])}
          >
            <img src={add} alt="" />
          </div>
        </div>
        <div className="edit-modal__bottom-btns">
          {!confirmDelete ? (
            <button
              className="edit-modal__bottom-btns__remove"
              onClick={() => {
                setConfirmDelete(true)
              }}
            >
              Удалить
            </button>
          ) : (
            <div className="edit-modal__bottom-btns__confirm">
              <button
                onClick={() => {
                  dispatch(closeModal())
                  setConfirmDelete(false)
                  if (product) {
                    dispatch(removeProductFromState(product))
                    service.delete('/product/' + product._id)
                  }
                }}
              >
                Удалить
              </button>
              <button onClick={() => setConfirmDelete(false)}>Отмена</button>
            </div>
          )}
          <button
            className="edit-modal__bottom-btns__save"
            onClick={handleSubmit}
          >
            Сохранить
          </button>
        </div>
      </div>
    </div>
  )
}
