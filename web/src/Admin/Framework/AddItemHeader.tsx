import React, { useState } from 'react'
import { useAppDispatch } from '../../store/hooks'
import AxiosService from '../../utils/axiosService'
import './Framework.scss'

interface Props {
  placeholder: string
  btnText: string
  route: string
  addAction: (
    payload: any
  ) => {
    payload: any
    type: string
  }
}

export const AddItemHeader = ({
  placeholder,
  btnText,
  route,
  addAction,
}: Props) => {
  const service = new AxiosService()
  const dispatch = useAppDispatch()
  const [newTitle, setNewTitle] = useState('')

  return (
    <div className="framework__add-item">
      <input
        type="text"
        className="framework__add-item__input"
        placeholder={placeholder}
        value={newTitle}
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
          setNewTitle(e.target.value)
        }}
      />
      <button
        className="framework__add-item__save-btn"
        onClick={async () => {
          if (newTitle.trim() !== '') {
            const res = await service.post(route, { title: newTitle }, true)
            res && dispatch(addAction(res))
            setNewTitle('')
          }
        }}
      >
        {btnText}
      </button>
    </div>
  )
}
