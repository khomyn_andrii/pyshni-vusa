import React, { useState } from 'react'
import trash from '../img/trash.svg'
import edit from '../img/edit.svg'
import save from '../img/save.svg'
import './Framework.scss'
import AxiosService from '../../utils/axiosService'
import { useAppDispatch } from '../../store/hooks'

interface Props {
  item: any
  route: string
  removeAction: (payload: any) => { payload: any; type: string }
}

export const RenameHeader = ({ item, route, removeAction }: Props) => {
  const dispatch = useAppDispatch()
  const [editTitle, setEditTitle] = useState(false)
  const [confirmDelete, setConfirmDelete] = useState(false)
  const [newTitle, setNewTitle] = useState(item.title || '')
  const [newPosition, setNewPosition] = useState(item.position || 999)

  const service = new AxiosService()

  return (
    <div className="framework__rename">
      {!editTitle ? (
        <>
          <img
            className="framework__rename__edit-btn"
            src={edit}
            alt=""
            onClick={() => {
              setEditTitle(true)
            }}
          />
          #{newPosition} {newTitle}
          {!confirmDelete ? (
            <img
              className="framework__rename__trash-btn"
              src={trash}
              alt=""
              onClick={() => setConfirmDelete(true)}
            />
          ) : (
            <div className="framework__rename__confirm-delete">
              <button
                className="confirm"
                onClick={() => {
                  service.delete(route + '/' + item._id)
                  dispatch(removeAction(item._id))
                }}
              >
                Удалить
              </button>
              <button
                className="cancel"
                onClick={() => setConfirmDelete(false)}
              >
                Отмена
              </button>
            </div>
          )}
        </>
      ) : (
        <>
          <img
            className="framework__rename__save-btn"
            src={save}
            alt=""
            onClick={() => {
              if (newTitle?.trim() !== '') {
                service.put(
                  route + '/' + item._id,
                  {
                    title: newTitle,
                    position: newPosition,
                  },
                  true
                )
                setEditTitle(false)
              }
            }}
          />
          <input
            type="number"
            className="framework__rename__position-input"
            value={Number(newPosition).toString()}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setNewPosition(Number(e.target.value))
            }
          />
          <input
            type="text"
            className="framework__rename__title-input"
            value={newTitle}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
              setNewTitle(e.target.value)
            }
          />
        </>
      )}
    </div>
  )
}
