import React, { useState } from 'react'
import { capitalize } from 'lodash'
import moment from 'moment'
import { IOrder, OrderStatusEnum } from '../../utils/types'
import AxiosService from '../../utils/axiosService'
import { calculateItemPrice } from '../../utils/calculateItemPrice'
import { calculateCartPrice } from '../../utils/calculateCartPrice'

interface Props {
  order: IOrder
  updateOrder: (id: string, status: OrderStatusEnum) => void
  removeOrderFromState: (id: string) => void
}

export const LiveCard = ({
  order,
  updateOrder,
  removeOrderFromState,
}: Props) => {
  const service = new AxiosService()
  const [confirmDelete, setConfirmDelete] = useState(false)

  const statuses: { [key: string]: string } = {
    new: 'новый',
    accepted: 'принят',
    cancelled: 'отменен',
    completed: 'завершен',
  }

  const changeStatus = async (id: string, status: string) => {
    await service.put('/order/' + id, { status }, true)
  }

  return (
    <div className="live__card">
      <div className="live__header">
        <span>
          <strong>Заказ #{order.key}</strong>
        </span>
        <span>
          Статус: <strong>{statuses[order.status]}</strong>
        </span>
      </div>
      <div className="live__grid">
        <div className="live__grid__part">
          <span>
            <strong>Доставка:</strong>{' '}
            {order.deliveryType === 'delivery' ? 'курьером' : 'самовывоз'}
          </span>
          {order.deliveryType !== 'pickup' && (
            <span>
              <strong>Адрес доставки:</strong> {order.address}
            </span>
          )}
          <span>
            <strong>Время:</strong>{' '}
            {capitalize(moment(order.date).format('dddd DD/MM HH:mm'))}
          </span>
          <span>
            <strong>Оплата:</strong>{' '}
            {order.paymentType === 'card' ? 'картой' : 'наличка'}
          </span>
          {!!order.changeFrom && (
            <span>
              <strong>Сдача с:</strong> {order.changeFrom}
            </span>
          )}
          <span>
            <strong>Имя:</strong> {order.customerName}
          </span>
          <span>
            <strong>Телефон:</strong> {order.customerPhone}
          </span>
          <span>
            <strong>E-mail:</strong> {order.customerEmail}
          </span>
          <span>
            <strong>Время создание заказа:</strong>{' '}
            {capitalize(moment(order.createdAt).format('dddd DD/MM HH:mm'))}
          </span>
          <span>
            <strong>Перезвонить для уточнения заказа:</strong>
            {order.callback ? ' Да' : ' Нет'}
          </span>
          <span>
            <strong>Комментарий:</strong> {order.comment}
          </span>
        </div>
        <div className="live__grid__part">
          {order.products.map((item, i) => (
            <div className="live__product" key={item.product.title + i}>
              <div className="live__product__title">
                {item.quantity}х {item.product.title} <br />(
                {item.choosenSize.title} - {item.choosenSize.diameter} см)
              </div>
              {item.additions.length > 0 && (
                <div className="live__product__additions">
                  <span>Дополнительно:</span>
                  <br />
                  {item.additions.map((item) => (
                    <div key={item._id}>
                      {item.title} - x{item.quantity}
                    </div>
                  ))}
                </div>
              )}
              <div className="live__product__price">
                {calculateItemPrice(item)} грн
              </div>
            </div>
          ))}

          <div className="live__total-price">
            Итого: {Number(calculateCartPrice(order).toFixed(2))} грн
          </div>
        </div>
      </div>
      <div className="live__btns">
        {!confirmDelete ? (
          <button
            className="live__btns__cancel"
            onClick={() => setConfirmDelete(true)}
          >
            Отменить
          </button>
        ) : (
          <div className="live__btns__confirm">
            <button
              className="live__btns__confirm__yes"
              onClick={() => {
                changeStatus(order._id, OrderStatusEnum.canceled)
                removeOrderFromState(order._id)
              }}
            >
              ✓
            </button>
            <button
              className="live__btns__confirm__no"
              onClick={() => setConfirmDelete(false)}
            >
              ✕
            </button>
          </div>
        )}

        {order.status === 'new' ? (
          <button
            className="live__btns__accept"
            onClick={() => {
              changeStatus(order._id, OrderStatusEnum.accepted)
              updateOrder(order._id, OrderStatusEnum.accepted)
            }}
          >
            Подтвердить
          </button>
        ) : (
          <button
            className="live__btns__complete"
            onClick={() => {
              changeStatus(order._id, OrderStatusEnum.completed)
              removeOrderFromState(order._id)
            }}
          >
            Завершить
          </button>
        )}
      </div>
    </div>
  )
}
