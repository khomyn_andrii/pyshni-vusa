/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import AxiosService from '../../utils/axiosService'
import { IOrder, OrderStatusEnum } from '../../utils/types'
import { LiveCard } from './LiveCard'
import './LiveOrders.scss'

export const LiveOrders = () => {
  const service = new AxiosService()
  const history = useHistory()
  const [orders, setOrders] = useState<IOrder[]>([])

  const removeOrderFromState = (id: string) => {
    setOrders(orders.filter((order) => order._id !== id))
  }

  const updateOrder = (id: string, status: OrderStatusEnum) => {
    setOrders(
      orders.map((order) => (order._id !== id ? order : { ...order, status }))
    )
  }

  const fetchData = async () => {
    try {
      const res: IOrder[] = await service.get('/order/live', {})
      res && res.sort((a, b) => b.key - a.key)
      setOrders(res)
    } catch (err) {
      history.push('/admin/login')
    }
  }

  useEffect(() => {
    fetchData()
    const interval = setInterval(async () => {
      fetchData()
    }, 20000)

    return () => clearInterval(interval)
  }, [])

  return (
    <div className="live">
      <div className="live__content">
        {orders && orders.length > 0 ? (
          orders.map((order) => (
            <LiveCard
              key={order.key}
              order={order}
              updateOrder={updateOrder}
              removeOrderFromState={removeOrderFromState}
            />
          ))
        ) : (
          <h2>Заказов пока нет!</h2>
        )}
      </div>
    </div>
  )
}
