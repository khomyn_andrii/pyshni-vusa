import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { store } from '../../store'
import { setError } from '../../store/error'
import axiosService from '../../utils/axiosService'
import './LoginPage.scss'

interface Props {
  setIsAuth: React.Dispatch<React.SetStateAction<boolean>>
}

export const LoginPage = ({ setIsAuth }: Props) => {
  const service = new axiosService()
  const [login, setLogin] = useState('')
  const [password, setPassword] = useState('')
  const history = useHistory()

  return (
    <div className="login-page">
      <div className="login-page__wrapper">
        <label>
          <p>Логин</p>
          <input
            type="text"
            value={login}
            onChange={(e: any) => setLogin(e.target.value)}
          />
        </label>
        <label>
          <p>Пароль</p>
          <input
            type="password"
            value={password}
            onChange={(e: any) => setPassword(e.target.value)}
          />
        </label>
        <div className="login-page__btn">
          <button
            type="submit"
            onClick={async () => {
              const res = await service.postWithoutToken('/auth/login', {
                name: login,
                password,
              })
              if (res && res.token) {
                window.localStorage.setItem('token', res.token)
                setIsAuth(true)
                history.push('/admin')
              } else {
                store.dispatch(setError('Неправильные данные'))
              }
            }}
          >
            Войти
          </button>
        </div>
      </div>
    </div>
  )
}
