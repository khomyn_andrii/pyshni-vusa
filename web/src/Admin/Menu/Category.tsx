import React from 'react'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { ICategory } from '../../utils/types'
import { AdminCard } from '../AdminCard'
import add from '../img/add.svg'
import './Category.scss'

import { openModal } from '../../store/modal'
import { RenameHeader } from '../Framework'
import { removeCategory } from '../../store/category'

interface Props {
  category: ICategory | undefined
}

export const Category = ({ category }: Props) => {
  const dispatch = useAppDispatch()
  const { products } = useAppSelector((state) => state.products)

  const categoryProducts = products
    .filter((product) => product.category === category?._id)
    .sort((a, b) => +b.available - +a.available)

  if (!category) {
    return null
  }

  return (
    <div className="admin-category">
      <RenameHeader
        route="/category"
        item={category}
        removeAction={removeCategory}
      />
      <div className="admin-category__content">
        <div
          className="admin-card"
          onClick={() => dispatch(openModal(category._id))}
        >
          <div className="admin-card__img-wrapper">
            <img src={add} alt="" className="admin-category__add-new" />
          </div>
        </div>
        {categoryProducts.map((product) => (
          <AdminCard product={product} key={product._id} />
        ))}
      </div>
    </div>
  )
}
