import React from 'react'
import { addCategory } from '../../store/category'
import { useAppSelector } from '../../store/hooks'
import { AddItemHeader } from '../Framework'
import { Category } from './Category'
import './Menu.scss'

export const Menu = () => {
  const { categories } = useAppSelector((state) => state.categories)

  return (
    <div className="admin-menu">
      <AddItemHeader
        placeholder="Новая категория"
        btnText="Добавить категорию"
        route="/category"
        addAction={addCategory}
      />
      {categories
        .slice()
        .sort((a, b) => a.position - b.position)
        .map((category) => (
          <Category category={category} key={category._id} />
        ))}
    </div>
  )
}
