import React, { useState } from 'react'
import { setError } from '../../store/error'
import { useAppDispatch } from '../../store/hooks'
import { endLoad, startLoad } from '../../store/loading'
import { removePromotion, replacePromotion } from '../../store/promotion'
import AxiosService from '../../utils/axiosService'
import noImage from '../../utils/noImage'
import { IPromotion } from '../../utils/types'

interface Props {
  promotion: IPromotion
}

export const PromoItem = ({ promotion }: Props) => {
  const dispatch = useAppDispatch()
  const service = new AxiosService()
  const isNewItem = Number.isInteger(+promotion._id)
  const [confirmRemove, setConfirmRemove] = useState(false)
  const [standartImage, setStandartImage] = useState(promotion.image)
  const [carouselImage, setCarouselImage] = useState(promotion.carouselImage)
  const [newDescription, setNewDescription] = useState(promotion.description)
  const [newTitle, setNewTitle] = useState(promotion.title)
  const [newPos, setNewPos] = useState(promotion.position)
  const [showMain, setShowMain] = useState(promotion.showMain)
  const fileInput = React.createRef<any>()
  const fileInputCarousel = React.createRef<any>()

  const handleSave = async () => {
    const refs = [fileInput, fileInputCarousel]
    const imageUrls = []

    if (!standartImage || !carouselImage) {
      return dispatch(setError('Фотография не должна быть пустой'))
    }

    if (!newTitle || !newDescription) {
      return dispatch(setError('Заполните поля'))
    }

    for (let idx = 0; idx < refs.length; idx++) {
      if (refs[idx] && refs[idx].current && refs[idx].current.files[0]) {
        const imageFile = refs[idx].current.files[0]
        const data = new FormData()
        data.append('file', imageFile)
        const imageUrl = await service.post('/upload/image', data)
        imageUrls[idx] = process.env.REACT_APP_SERVER + '/' + imageUrl.path
      }
    }

    const updPromo = {
      title: newTitle,
      showMain: showMain,
      position: newPos,
      description: newDescription,
      image: imageUrls[0] || promotion.image,
      carouselImage: imageUrls[1] || promotion.carouselImage,
    }

    try {
      dispatch(startLoad())
      if (isNewItem) {
        const res = await service.post('/promotion/', { ...updPromo }, true)
        dispatch(replacePromotion({ promotion: res, oldId: promotion._id }))
      } else {
        await service.put('/promotion/' + promotion._id, updPromo, true)
      }
    } catch (err) {
      dispatch(setError('Что-то пошло не так. Попробуйте еще раз позже'))
      console.log(err.message)
    } finally {
      setTimeout(() => dispatch(endLoad()), 500)
    }
  }

  const handleRemove = async () => {
    try {
      if (!isNewItem) {
        dispatch(startLoad())
        const res = await service.delete('/promotion/' + promotion._id)
        res && dispatch(removePromotion(promotion._id))
      } else {
        dispatch(removePromotion(promotion._id))
      }
    } catch (err) {
      dispatch(setError('Что-то пошло не так. Попробуйте еще раз позже'))
      console.log(err.message)
    } finally {
      setTimeout(() => dispatch(endLoad()), 500)
    }
  }

  return (
    <div className="promo-section__item">
      <div className="promo-section__title">
        <label className="position">
          Позиция
          <input
            type="number"
            value={newPos}
            onChange={(e: any) => setNewPos(e.target.value)}
            placeholder="Название акции"
          />
        </label>
        <label className="title">
          Название акции
          <input
            value={newTitle}
            onChange={(e: any) => setNewTitle(e.target.value)}
            placeholder="Название акции"
          />
        </label>
      </div>
      <div className="promo-section__description">
        <label>
          Описание акции
          <textarea
            placeholder="Описание акции"
            value={newDescription}
            onChange={(e: React.ChangeEvent<HTMLTextAreaElement>) =>
              setNewDescription(e.target.value)
            }
          />
        </label>
      </div>
      <div className="promo-section__check">
        <label>
          <input
            type="checkbox"
            checked={showMain}
            onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
              setShowMain(e.target.checked)
            }}
          />
          Показывать на главной
        </label>
      </div>
      <div className="promo-section__grid">
        <div className="promo-section__grid__item">
          <div className="promo-section__header">Для банера</div>
          <img
            alt=""
            src={carouselImage}
            className="promo-section__img carousel"
          />
          <div className="promo-section__btn">
            <label>
              Выбрать фото
              <input
                type="file"
                ref={fileInputCarousel}
                accept="image/*"
                onChange={() => {
                  const param = fileInputCarousel.current.files[0]
                  const res = URL.createObjectURL(param)
                  setCarouselImage(res)
                }}
              />
            </label>
          </div>
        </div>
        <div className="promo-section__grid__item">
          <div className="promo-section__header">Для акций</div>
          <div
            className="promo-section__img standart"
            style={{
              backgroundImage: `url(${standartImage}), url(${noImage})`,
            }}
          />
          <div className="promo-section__btn">
            <label>
              Выбрать фото
              <input
                type="file"
                ref={fileInput}
                accept="image/*"
                onChange={() => {
                  const param = fileInput.current.files[0]
                  const res = URL.createObjectURL(param)
                  setStandartImage(res)
                }}
              />
            </label>
          </div>
        </div>
      </div>
      <div className="promo-section__action-btns">
        {!confirmRemove ? (
          <>
            <button className="promo-section__save-btn" onClick={handleSave}>
              Сохранить
            </button>
            <button
              className="promo-section__remove-btn"
              onClick={() => setConfirmRemove(true)}
            >
              Удалить
            </button>
          </>
        ) : (
          <>
            <button
              className="promo-section__remove-btn"
              onClick={handleRemove}
            >
              Подтвердить
            </button>
            <button
              className="promo-section__cancel-btn"
              onClick={() => setConfirmRemove(false)}
            >
              Отмена
            </button>
          </>
        )}
      </div>
    </div>
  )
}
