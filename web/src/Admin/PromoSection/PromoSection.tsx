import React from 'react'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { PromoItem } from './PromoItem'
import { IPromotion } from '../../utils/types'
import addIcon from '../img/add.svg'
import './PromoSection.scss'
import { addPromotion } from '../../store/promotion'

export const PromoSection = () => {
  const dispatch = useAppDispatch()
  const { promotions } = useAppSelector((state) => state.promotions)

  const emptyPromotion: IPromotion = {
    _id: '',
    title: '',
    description: '',
    image: '',
    carouselImage: '',
    position: 100,
    showMain: false,
  }

  let key = 0

  return (
    <div className="promo-section">
      {promotions.length &&
        promotions.map((promotion) => (
          <PromoItem promotion={promotion} key={promotion._id || key++} />
        ))}
      <div className="promo-section__add-btn">
        <img
          src={addIcon}
          alt=""
          onClick={() => {
            let id = promotions.length + 1
            dispatch(addPromotion({ ...emptyPromotion, _id: id.toString() }))
          }}
        />
      </div>
    </div>
  )
}
