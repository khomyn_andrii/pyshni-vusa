import React from 'react'
import './SimpleNav.scss'

interface Props {
  isOpen: boolean
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export const SimpleNav = ({ isOpen, setIsOpen }: Props) => {
  return (
    <div className="simpleNav">
      <div
        className={'simpleNav__btn' + (isOpen ? ' open' : '')}
        onClick={() => setIsOpen(!isOpen)}
      >
        <span></span>
        <span></span>
        <span></span>
      </div>
    </div>
  )
}
