import { Editor } from '@tinymce/tinymce-react'
import React from 'react'

interface EditorProps {
  editorValue: string
  setEditorValue: React.Dispatch<React.SetStateAction<string>>
}

export const HTMLEditor = ({ editorValue, setEditorValue }: EditorProps) => {
  const handleEditorChange = (content: string) => {
    setEditorValue(content)
  }

  return (
    <Editor
      value={editorValue}
      init={{
        height: 250,
        menubar: false,
        plugins: [
          'advlist autolink lists link image',
          'charmap print preview anchor help',
          'searchreplace visualblocks code',
          'insertdatetime media table paste wordcount',
        ],
        toolbar: [
          'undo redo | formatselect | fontsizeselect | lineheight | bold italic strikethrough underline  | subscript superscript | alignleft aligncenter alignright | bullist numlist outdent indent | help',
        ],
      }}
      onEditorChange={handleEditorChange}
    />
  )
}
