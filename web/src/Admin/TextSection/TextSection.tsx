/* eslint-disable react-hooks/exhaustive-deps */
import React, { useRef, useState } from 'react'
import './TextSection.scss'
import { Field, Form, Formik, FormikProps } from 'formik'
import { HTMLEditor } from './HTMLEditor'
import { ITexts } from '../../utils/types'
import AxiosService from '../../utils/axiosService'
import { useAppDispatch } from '../../store/hooks'
import { endLoad, startLoad } from '../../store/loading'
import { setError } from '../../store/error'
import noImage from '../../utils/noImage'

interface Props {
  texts: ITexts
}

export const TextSection = ({ texts }: Props) => {
  const dispatch = useAppDispatch()
  const service = new AxiosService()
  const fileInput = useRef<HTMLInputElement>(null)

  const [image, setImage] = useState(texts.deliveryImage)

  return (
    <>
      <div className="text-section">
        <Formik
          enableReinitialize
          initialValues={{
            firstPhone: texts.firstPhone || '',
            secondPhone: texts.secondPhone || '',
            thirdPhone: texts.thirdPhone || '',
            startWorkTime: texts.startWorkTime || '',
            endWorkTime: texts.endWorkTime || '',
            contactsText: texts.contactsText || '',
            seoText: texts.seoText || '',
            promotionText: texts.promotionText || '',
            address: texts.address || '',
            secondAddress: texts.secondAddress || '',
            mapLink: texts.mapLink || '',
            facebook: texts.facebook || '',
            instagram: texts.instagram || '',
            telegram: texts.telegram || '',
            minOrderTime: texts.minOrderTime || 30,
            deliveryImage: texts.deliveryImage || '',
            deliveryText: texts.deliveryText || '',
          }}
          onSubmit={async (values) => {
            try {
              dispatch(startLoad())
              let path
              if (
                fileInput.current &&
                fileInput.current.files &&
                fileInput.current.files[0]
              ) {
                const imageFile = fileInput.current.files[0]
                const data = new FormData()
                data.append('file', imageFile)
                const imageUrl = await service.post('/upload/image', data)
                path = process.env.REACT_APP_SERVER + '/' + imageUrl.path
              }

              await service.put(
                '/texts',
                { ...values, deliveryImage: path || texts.deliveryImage },
                true
              )
            } catch (error) {
              dispatch(setError('Не все поля введены'))
            } finally {
              dispatch(endLoad())
            }
          }}
        >
          {({ values, setFieldValue }: FormikProps<ITexts>) => (
            <Form>
              <label className="text-section__label">
                Первый номер телефона:
                <Field name="firstPhone" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Второй номер телефона:
                <Field name="secondPhone" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Третий номер телефона:
                <Field name="thirdPhone" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Instagram:
                <Field name="instagram" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Facebook:
                <Field name="facebook" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Telegram:
                <Field name="telegram" className="text-section__input" />
              </label>
              <div className="text-section__time">
                <label className="text-section__label">
                  Время открытия:
                  <Field name="startWorkTime" className="text-section__input" />
                </label>
                <label className="text-section__label">
                  Время закрытия:
                  <Field name="endWorkTime" className="text-section__input" />
                </label>
                <label className="text-section__label">
                  Минимальное время между заказами:
                  <Field
                    type="number"
                    name="minOrderTime"
                    className="text-section__input"
                  />
                </label>
              </div>
              <label className="text-section__label">
                Текст “О нас”
                <HTMLEditor
                  editorValue={values.seoText}
                  setEditorValue={(content) =>
                    setFieldValue('seoText', content)
                  }
                />
              </label>
              <label className="text-section__label">
                Текст “Контакти”
                <HTMLEditor
                  editorValue={values.contactsText}
                  setEditorValue={(content) =>
                    setFieldValue('contactsText', content)
                  }
                />
              </label>
              <label className="text-section__label">
                Ссылка на карту:
                <Field name="mapLink" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Адресс:
                <Field name="address" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Второй Адресс:
                <Field name="secondAddress" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Рекламный текст снизу (в красном прямогульнике):
                <Field name="promotionText" className="text-section__input" />
              </label>
              <label className="text-section__label">
                Фотография для экрана "Доставка":
              </label>

              <div
                className="text-section__delivery-img"
                style={{
                  backgroundImage: `url(${
                    image || texts.deliveryImage
                  }), url(${noImage})`,
                }}
              />
              <label className="text-section__label img-download">
                <p>Выбрать фото</p>
                <input
                  type="file"
                  ref={fileInput}
                  accept="image/*"
                  onChange={() => {
                    if (
                      fileInput &&
                      fileInput.current &&
                      fileInput.current.files
                    ) {
                      const param = fileInput.current.files[0]
                      const res = URL.createObjectURL(param)
                      setImage(res)
                    }
                  }}
                />
              </label>

              <label className="text-section__label">
                Текст для экрана “Доставка”
                <HTMLEditor
                  editorValue={values.deliveryText}
                  setEditorValue={(content) =>
                    setFieldValue('deliveryText', content)
                  }
                />
              </label>
              <div className="text-section__btn">
                <button type="submit">Сохранить</button>
              </div>
            </Form>
          )}
        </Formik>
      </div>
    </>
  )
}
