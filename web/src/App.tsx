/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react'
import { Carousel } from './components/Carousel'
import { Switch, Route, useLocation, Redirect } from 'react-router-dom'
import { Admin } from './Admin'
import { Navbar } from './components/Navbar'
import { Menu } from './components/Menu/Menu'
import { Footer } from './components/Footer'
import { Promotions } from './components/Promotions'
import axiosService from './utils/axiosService'
import { useAppDispatch, useAppSelector } from './store/hooks'
import { setProducts } from './store/product'
import { setCategories } from './store/category'
import { endLoad, startLoad } from './store/loading'
import { setPromotions } from './store/promotion'
import logoLoader from './img/logoLoader.svg'
import mustach from './img/mustach.svg'
import { AboutUs } from './components/Footer/AboutUs'
import { Contacts } from './components/Contacts'
import { Order } from './components/Order'
import { Burger } from './components/Burger'
import { setAdditionCategories } from './store/additionCategory'
import { GlobalError } from './components/GlobalError'
import { setTexts } from './store/texts'
import { ProductPage } from './components/ProductPage'
import { PromotionPage } from './components/Promotions/PromotionPage'
import { DeliveryPage } from './components/DeliveryPage'
import { NotFound } from './components/NotFound'

declare global {
  interface Window { dataLayer: any[]; }
}

const ScrollToTop = () => {
  const { pathname } = useLocation()

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [pathname])

  return null
}

interface AppProps {
  isHydrate: boolean
}

export default function App({ isHydrate }: AppProps) {
  const dispatch = useAppDispatch()
  const { loading } = useAppSelector((state) => state.loading)
  const { additionCategories } = useAppSelector(
    (state) => state.additionCategories
  )

  const service = new axiosService()

  const [hasError, setHasError] = useState(false)
  const [openBurger, setOpenBurger] = useState(false)
  const [hasMounted, setHasMounted] = useState(false)

  useEffect(() => {
    setHasMounted(true)
    const fetchData = async () => {
      try {
        dispatch(startLoad())
        const productsData = await service.get('/product', {})
        const categoriesData = await service.get('/category', {})
        const promotionsData = await service.get('/promotion', {})
        const additionCategoriesData = await service.get(
          '/addition_category',
          {}
        )
        const textsData = await service.get('/texts', {})
        dispatch(setProducts(productsData || []))
        dispatch(setCategories(categoriesData || []))
        dispatch(setPromotions(promotionsData || []))
        dispatch(setAdditionCategories(additionCategoriesData || []))
        dispatch(setTexts(textsData || {}))
        dispatch(endLoad())
      } catch (err) {
        setHasError(true)
        dispatch(endLoad())
      }
    }
    fetchData()
  }, [])

  return (
    <>
      {hasMounted && (
        <>
          <ScrollToTop />
          <div className={'loader' + (loading ? ' shown' : '')}>
            <div className="myLoader">
              <img src={logoLoader} alt="" />
              <div className="mustach">
                <img src={mustach} alt="" />
              </div>
            </div>
          </div>
          <GlobalError />
          <Switch>
            <Route path="/admin" component={Admin} />
            <Route path="/">
              <Navbar isOpen={openBurger} setIsOpen={setOpenBurger} />
              <Burger isOpen={openBurger} setIsOpen={setOpenBurger} />
              <div className="container">
                <Switch>
                  {hasError && (
                    <Route
                      path="*"
                      render={() => (
                        <NotFound text="Технические неполадки :(" />
                      )}
                    />
                  )}
                  <Route exact path="/">
                    {isHydrate && <Carousel />}
                    <Menu />
                    <AboutUs />
                  </Route>
                  <Route
                    path="/menu/:id"
                    component={() => (
                      <ProductPage additionCategories={additionCategories} />
                    )}
                  />
                  <Route exact path="/menu" component={Menu} />
                  <Route exact path="/promotions" component={Promotions} />
                  <Route path="/promotions/:id" component={PromotionPage} />
                  <Route path="/contacts" component={Contacts} />
                  <Route path="/order" component={Order} />
                  <Route path="/delivery" component={DeliveryPage} />
                  <Route
                    path="*"
                    render={() => <NotFound text="Страница не найдена :(" />}
                  />
                </Switch>
              </div>
              <Footer />
            </Route>
            <Redirect to="/404" />
          </Switch>
        </>
      )}
    </>
  )
}
