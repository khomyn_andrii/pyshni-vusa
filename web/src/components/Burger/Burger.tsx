import React from 'react'
import { useHistory } from 'react-router-dom'
import menuRed from '../../img/menu-book-red.svg'
import percentRed from '../../img/percent-red.svg'
import phoneRed from '../../img/phone-red.svg'
import truckRed from '../../img/truck-red.svg'
import './Burger.scss'

interface Props {
  isOpen: boolean
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export const Burger = ({ isOpen, setIsOpen }: Props) => {
  const history = useHistory()

  return (
    <div className={'burger' + (isOpen ? ' active' : '')}>
      <div className="burger__wrapper">
        <div className="burger__content">
          <div
            className="burger__content__item"
            onClick={() => {
              history.push('/menu')
              setIsOpen(false)
            }}
          >
            <img src={menuRed} alt="" />
            <p>Меню</p>
          </div>
          <div
            className="burger__content__item"
            onClick={() => {
              history.push('/promotions')
              setIsOpen(false)
            }}
          >
            <img src={percentRed} alt="" />
            <p>Акції</p>
          </div>
          <div
            className="burger__content__item"
            onClick={() => {
              history.push('/delivery')
              setIsOpen(false)
            }}
          >
            <img src={truckRed} alt="" />
            <p>Доставка</p>
          </div>
          <div
            className="burger__content__item"
            onClick={() => {
              history.push('/contacts')
              setIsOpen(false)
            }}
          >
            <img src={phoneRed} alt="" />
            <p>Контакти</p>
          </div>
        </div>
      </div>
    </div>
  )
}
