import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { addProduct, removeOneByKey, addOneByKey } from '../../store/cart'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import noImage from '../../utils/noImage'
import { IProduct } from '../../utils/types'
import './Card.scss'

interface Props {
  product: IProduct
}

export const Card = ({ product }: Props) => {
  const dispatch = useAppDispatch()
  const history = useHistory()
  const { addedProducts } = useAppSelector((state) => state.cart)
  const [active, setActive] = useState<number>(0)
  const [togglePos, setTogglePos] = useState(0)

  if (!product) {
    return null
  }

  const sizesCopy = product.sizes.slice()

  const sizes = sizesCopy.sort((a, b) => a.diameter - b.diameter)
  const fraction = 100 / sizes.length
  let start = 0 - fraction
  const percents = sizes.map(() => Math.round((start += fraction)))

  const productInCart = addedProducts.find(
    (cartProduct) =>
      cartProduct.product._id === product._id &&
      cartProduct.choosenSize._id === sizes[active]._id
  )

  return (
    <div className="card">
      <div
        className="card__image"
        style={{
          backgroundImage: `url(${product.image}), url(${noImage})`,
        }}
        onClick={() => history.push('/menu/' + product._id)}
      ></div>
      <div className="card__weight">
        {Number(sizes[active].weight)}{' '}
        {product.weightTypes === 'gram' ? 'гр' : 'мл'}
      </div>
      {sizes.length <= 1 && sizes[0].diameter > 0 ? (
        <div className="card__weight">{sizes[0].diameter} см</div>
      ) : null}
      <div
        className="card__title"
        onClick={() => history.push('/menu/' + product._id)}
      >
        {product.title}
      </div>
      <div className="card__description">{product.description}</div>
      <div className="card__height-fix">
        {sizes.length <= 1 ? null : (
          <div className="card__size-choose">
            {sizes.map((size, i) => (
              <div
                className={
                  'card__size-choose__item' + (active === i ? ' active' : '')
                }
                style={{ width: fraction + '%' }}
                key={i}
                onClick={() => {
                  setTogglePos(percents[i])
                  setActive(i)
                }}
              >
                <p>
                  {+size.diameter === 0 ? '' : size.diameter + ' -'}{' '}
                  {size.title}
                </p>
              </div>
            ))}
            <div
              className="card__size-toggle"
              style={{ left: togglePos + '%', width: fraction + '%' }}
            ></div>
          </div>
        )}
        <div className="card__bottom">
          <div className="card__bottom__price">
            {Number(sizes[active].price)} <span>грн</span>
          </div>
          {productInCart ? (
            <div className="card__bottom__cart-btns">
              <button
                onClick={() =>
                  dispatch(removeOneByKey({ key: productInCart.key }))
                }
              >
                <span className="minus">_</span>
              </button>
              <span> {productInCart.quantity} </span>
              <button
                onClick={() => {
                  dispatch(addOneByKey({ key: productInCart.key }))
                  window.dataLayer.push({ event: 'AddToCart' })
                }}
              >
                <span>+</span>
              </button>
            </div>
          ) : (
            <button
              className="card__bottom__add-btn"
              onClick={() => {
                dispatch(addProduct({ product, choosenSize: sizes[active] }))
                window.dataLayer.push({ event: 'AddToCart' })
              }}
            >
              В корзину
            </button>
          )}
        </div>
      </div>
    </div>
  )
}
