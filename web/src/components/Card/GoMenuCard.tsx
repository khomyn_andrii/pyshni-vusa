import React from 'react'
import { useHistory } from 'react-router-dom'
import { ReactComponent as MenuArrow } from '../../img/goMenuArrow.svg'

interface Props {
  activeId: number
}

export const GoMenuCard = ({ activeId }: Props) => {
  const history = useHistory()

  return (
    <div className="card">
      <section
        className="card__go-menu"
        onClick={() => history.push('/menu?pos=' + activeId)}
      >
        <p className="card__go-menu__header">Переглянути повне меню</p>
        <MenuArrow />
      </section>
    </div>
  )
}
