import React from 'react'
import AliceCarousel from 'react-alice-carousel'
import { useHistory } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'
import './Carousel.scss'

const handleDragStart = (e: any) => e.preventDefault()

export const Carousel = () => {
  const { promotions } = useAppSelector((state) => state.promotions)
  const history = useHistory()

  if (!promotions.length || !promotions[0]._id) {
    return null
  }

  const items = promotions
    .filter((p) => p.showMain)
    .sort((a, b) => a.position - b.position)
    .map((promotion) => (
      <img
        alt=""
        src={promotion.carouselImage}
        onDragStart={handleDragStart}
        onClick={() => history.push('/promotions/' + promotion._id)}
        style={{ cursor: 'pointer' }}
      />
    ))

  return (
    <div>
      <AliceCarousel
        disableButtonsControls
        infinite
        autoPlay
        autoPlayInterval={3000}
        items={items}
      />
    </div>
  )
}
