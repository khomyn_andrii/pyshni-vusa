import React from 'react'
import locationRed from '../../img/location-red.svg'
import phoneRed from '../../img/phone-red.svg'
import clockRed from '../../img/clock-red.svg'
import instaRed from '../../img/insta-red.svg'
import facebookRed from '../../img/facebook-red.svg'
import telegramRed from '../../img/telegram-red.svg'
import ReactHtmlParser from 'react-html-parser'
import { useAppSelector } from '../../store/hooks'
import './Contacts.scss'

export const Contacts = () => {
  const { texts } = useAppSelector((state) => state.texts)

  return (
    <div className="contacts">
      <div className="contacts__header">Контакти</div>
      <div className="contacts__content">
        <div className="contacts__map">
          <iframe
            src={texts.mapLink}
            width="100%"
            height="100%"
            title="Map"
            style={{ border: 'none' }}
            loading="lazy"
          ></iframe>
        </div>
        <div className="contacts__list">
          <ul>
            <li>
              <div className="contacts__list__header">Приходи к нам:</div>
              <div className="contacts__list__subfield">
                <img src={locationRed} alt="" />
                <span>{texts.address}</span>
              </div>
              {texts.secondAddress && 
              <div className="contacts__list__subfield">
                <img src={locationRed} alt="" />
                <span>{texts.secondAddress}</span>
              </div>}
              <div className="contacts__list__subfield">
                <img src={clockRed} alt="" />
                <span>
                  {texts.startWorkTime} - {texts.endWorkTime}
                </span>
              </div>
            </li>
            <li>
              <div className="contacts__list__header">
                Позвонить можно сюда:
              </div>
              <div className="contacts__list__subfield">
                <img src={phoneRed} alt="" />
                <a href={`tel:${texts.firstPhone}`}>{texts.firstPhone}</a>
              </div>
              {texts.secondPhone && (
                <div className="contacts__list__subfield">
                  <img src={phoneRed} alt="" />
                  <a href={`tel:${texts.secondPhone}`}>{texts.secondPhone}</a>
                </div>
              )}
            </li>
            <li>
              <div className="contacts__list__header">
                Мы в социальных сетях:
              </div>
              <div className="contacts__list__subfield">
                {texts.instagram && 
                <a target="_blank" rel="noreferrer" href={texts.instagram}>
                  <img
                    className="contacts__list__subfield__social"
                    src={instaRed}
                    alt=""
                  />
                </a>}
                {texts.facebook &&   
                <a target="_blank" rel="noreferrer" href={texts.facebook}>
                  <img
                    className="contacts__list__subfield__social"
                    src={facebookRed}
                    alt=""
                  />
                </a>}
                {texts.telegram &&  
                <a target="_blank" rel="noreferrer" href={texts.telegram}>
                  <img
                    className="contacts__list__subfield__social"
                    src={telegramRed}
                    alt=""
                  />
                </a>}
              </div>
            </li>
            <li>{ReactHtmlParser(texts.contactsText)}</li>
          </ul>
        </div>
      </div>
    </div>
  )
}
