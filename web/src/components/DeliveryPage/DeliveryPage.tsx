import React from 'react'
import { useAppSelector } from '../../store/hooks'
import ReactHtmlParser from 'react-html-parser'
import './DeliveryPage.scss'
import noImage from '../../utils/noImage'

export const DeliveryPage = () => {
  const { texts } = useAppSelector((state) => state.texts)

  return (
    <div className="delivery-page">
      <div className="delivery-page__header">Доставка</div>
      <div className="delivery-page__grid">
        <div className="delivery-page__img">
          <div
            className="image"
            style={{
              backgroundImage: `url(${texts.deliveryImage}), url(${noImage})`,
            }}
          />
        </div>
        <div className="delivery-page__text">
          {ReactHtmlParser(texts.deliveryText)}
        </div>
      </div>
    </div>
  )
}
