/* eslint-disable react-hooks/exhaustive-deps */
import React, { useCallback, useEffect, useRef } from 'react'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import closeBtn from '../../img/close-btn.svg'
import {
  addOneByKey,
  removeOneByKey,
  removeProductByKey,
} from '../../store/cart'
import './Dropdown.scss'
import { useHistory } from 'react-router-dom'
import { calculateItemPrice } from '../../utils/calculateItemPrice'
import noImage from '../../utils/noImage'

interface Props {
  showMenu: boolean
  setShowMenu: React.Dispatch<React.SetStateAction<boolean>>
}

export const Dropdown = ({ showMenu, setShowMenu }: Props) => {
  const history = useHistory()
  const { addedProducts } = useAppSelector((state) => state.cart)
  const refContainer = useRef<HTMLDivElement>({} as HTMLDivElement)
  const dispatch = useAppDispatch()

  const handleClose = useCallback((e: Event | null) => {
    if (!e) {
      setShowMenu(false)
      document.removeEventListener('click', handleClose)
    } else {
      const elem = e.target as HTMLElement
      if (
        refContainer.current &&
        !elem.parentElement?.className.includes('dropdown')
      ) {
        setShowMenu(false)
        document.removeEventListener('click', handleClose)
      }
    }
  }, [])

  useEffect(() => {
    if (showMenu) {
      document.addEventListener('click', handleClose)
    }
  }, [handleClose, showMenu])

  if (!addedProducts.length) {
    return null
  }

  return (
    <div className={'dropdown' + (showMenu ? ' open' : '')}>
      <div className="dropdown__content" ref={refContainer}>
        {addedProducts.length
          ? addedProducts.map((item) => {
              return (
                <div key={item.key}>
                  <div className="dropdown__content__item">
                    <div
                      className="dropdown__content__item__img"
                      style={{
                        backgroundImage: `url(${item.product.image}),  url(${noImage})`,
                      }}
                    ></div>
                    <div className="dropdown__content__item__text">
                      <div className="dropdown__content__item__text__title">
                        {item.product.title}
                      </div>
                      <div className="dropdown__content__item__text__size">
                        {item.choosenSize.title}{' '}
                        {item.choosenSize.diameter > 0
                          ? ` - ${item.choosenSize.diameter}`
                          : ''}
                      </div>
                      <div className="dropdown__content__item__text__description">
                        {item.product.description}
                      </div>
                    </div>
                    <div
                      className="dropdown__content__item__close-btn"
                      onClick={() =>
                        dispatch(removeProductByKey({ key: item.key }))
                      }
                    >
                      <img src={closeBtn} alt="" />
                    </div>
                  </div>
                  <div className="dropdown__content__bottom">
                    <div className="dropdown__content__bottom__price">
                      {calculateItemPrice(item)} грн
                    </div>
                    <div className="dropdown__content__bottom__buttons">
                      <button
                        className="dropdown__content__bottom_btn"
                        onClick={(e: any) => {
                          e.preventDefault()
                          dispatch(removeOneByKey({ key: item.key }))
                        }}
                      >
                        <span className="minus">_</span>
                      </button>
                      <span>{item.quantity}</span>
                      <button
                        className="dropdown__content__bottom_btn"
                        onClick={() => dispatch(addOneByKey({ key: item.key }))}
                      >
                        <span>+</span>
                      </button>
                    </div>
                  </div>
                </div>
              )
            })
          : null}
        <div className="dropdown__order-btn">
          <button
            onClick={() => {
              setShowMenu(false)
              history.push('/order')
            }}
          >
            Заказать
          </button>
        </div>
      </div>
    </div>
  )
}
