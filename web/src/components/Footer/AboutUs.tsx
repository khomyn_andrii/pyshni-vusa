import React from 'react'
import { useAppSelector } from '../../store/hooks'
import ReactHtmlParser from 'react-html-parser'
import './AboutUs.scss'

export const AboutUs = () => {
  const { texts } = useAppSelector((state) => state.texts)

  const image =
    'https://chefsmandala.com/wp-content/uploads/2018/04/Pizza-Dough-Making.jpg'

  return (
    <div className="about">
      <div className="about__header">О нас</div>
      <div className="about__section">
        <div
          className="about__section__img"
          style={{ backgroundImage: `url(${image})` }}
        ></div>
        <div className="about__section__text">
          {ReactHtmlParser(texts.seoText)}
        </div>
      </div>
    </div>
  )
}
