import React from 'react'
import logoWhite from '../../img/logoWhite.svg'
import facebook from '../../img/faceb.svg'
import insta from '../../img/insta.svg'
import teleg from '../../img/teleg.svg'
import facebookGray from '../../img/facebook-gray.svg'
import instaGray from '../../img/insta-gray.svg'
import telegGray from '../../img/teleg-gray.svg'
import mastercard from '../../img/mastercard.svg'
import visa from '../../img/visa.svg'
import arrowDownGray from '../../img/arrowDownGray.svg'
import './Footer.scss'
import { useAppSelector } from '../../store/hooks'
import { useHistory } from 'react-router-dom'

const handleClick = (e: React.MouseEvent<HTMLButtonElement>) => {
  e.preventDefault()
  e.currentTarget.classList.toggle('active')
}

export const Footer = () => {
  const { texts } = useAppSelector((state) => state.texts)
  const history = useHistory()

  return (
    <div className="footer">
      <div className="footer__promo-text">{texts.promotionText}</div>
      <section className="desktop">
        <div className="footer__wrapper">
          <div className="footer__item __first">
            <img
              className="footer__item__logo"
              src={logoWhite}
              alt=""
              onClick={() => history.push('/')}
            />
            <div className="footer__item__links">
              {texts.instagram && (
                <a target="_blank" rel="noreferrer" href={texts.instagram}>
                  <img src={instaGray} alt="" />
                </a>
              )}
              {texts.facebook && (
                <a target="_blank" rel="noreferrer" href={texts.facebook}>
                  <img src={facebookGray} alt="" />
                </a>
              )}
              {texts.telegram && (
                <a target="_blank" rel="noreferrer" href={texts.telegram}>
                  <img src={telegGray} alt="" />
                </a>
              )}
            </div>
          </div>
          <div className="footer__item __second">
            <ul>
              <li className="__list-header" onClick={() => history.push('/')}>
                Піца та Суші
              </li>
              <li onClick={() => history.push('/menu')}>Меню</li>
              <li onClick={() => history.push('/promotions')}>Акції</li>
              <li onClick={() => history.push('/delivery')}>Доставка</li>
            </ul>
          </div>
          <div className="footer__item __third">
            <div className="footer__item__header">Ми приймаємо</div>
            <div className="footer__item__payments">
              <img src={mastercard} alt="" />
              <img src={visa} alt="" />
            </div>
          </div>
          <div className="footer__item __fourth">
            <div className="footer__item__header">Контакти</div>
            <div className="footer__item__contacts">
              <a href={`tel:${texts.firstPhone}`}>{texts.firstPhone}</a>
              </div>
            {texts.secondPhone && (
              <div className="footer__item__contacts">
                <a href={`tel:${texts.secondPhone}`}>{texts.secondPhone}</a></div>
            )}
            <div className="footer__item__contacts">{texts.address}</div>
          </div>
        </div>
      </section>

      <section className="mobile">
        <div className="footer__wrapper">
          <div className="footer__accordion">
            <button className="footer__accordion__btn" onClick={handleClick}>
              PYSHNI VUSA
              <img src={arrowDownGray} alt=""></img>
            </button>
            <div className="footer__accordion__panel">
              <div className="footer__item __bigger __second">
                <ul>
                  <li
                    className="__list-header"
                    onClick={() => history.push('/')}
                  >
                    Піца та суші
                  </li>
                  <li onClick={() => history.push('/menu')}>Меню</li>
                  <li onClick={() => history.push('/promotions')}>Акції</li>
                  <li onClick={() => history.push('/delivery')}>Доставка</li>
                </ul>
              </div>
            </div>
            <button
              className="footer__accordion__btn __border-top"
              onClick={handleClick}
            >
              КОНТАКТИ
              <img src={arrowDownGray} alt=""></img>
            </button>
            <div className="footer__accordion__panel ">
              <div className="footer__item __fourth __bigger">
                <ul>
                  <li className="footer__item__header">Контакти</li>
                  <li className="footer__item__contacts">
                    <a href={`tel:${texts.firstPhone}`}>{texts.firstPhone}</a>
                  </li>
                  {texts.secondPhone && (
                    <li className="footer__item__contacts">
                      <a href={`tel:${texts.secondPhone}`}>{texts.secondPhone}</a>
                    </li>
                  )}
                  <li className="footer__item__contacts">{texts.address}</li>
                </ul>
              </div>
            </div>
          </div>
          <div className="footer__section">
            <p>Ми в соціальних мережах</p>
            <div className="footer__section__icons">
              {texts.instagram && (
                <a target="_blank" rel="noreferrer" href={texts.instagram}>
                  <img src={insta} alt="" />
                </a>
              )}
              {texts.facebook && (
                <a target="_blank" rel="noreferrer" href={texts.facebook}>
                  <img src={facebook} alt="" />
                </a>
              )}
              {texts.telegram && (
                <a target="_blank" rel="noreferrer" href={texts.telegram}>
                  <img src={teleg} alt="" />
                </a>
              )}
            </div>
          </div>
          <div className="footer__section">
            <p>Ми приймаємо</p>
            <div className="footer__section__icons payments">
              <img src={visa} alt="" />
              <img src={mastercard} alt="" />
            </div>
          </div>
        </div>
      </section>
    </div>
  )
}
