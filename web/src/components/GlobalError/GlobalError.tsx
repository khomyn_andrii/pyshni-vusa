/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import redCross from '../../img/red-cross.svg'
import './GlobalError.scss'
import { setError } from '../../store/error'

let timer: NodeJS.Timeout

export const GlobalError = () => {
  const dispatch = useAppDispatch()
  const { errorText } = useAppSelector((state) => state.error)

  useEffect(() => {
    if (timer) {
      clearTimeout(timer)
    }
    timer = setTimeout(() => dispatch(setError('')), 5000)
  }, [errorText])

  return (
    <div className={'global-error' + (errorText.trim() !== '' ? ' shown' : '')}>
      <div className="global-error__red-line" />
      <div className="global-error__red-cross">
        <img src={redCross} alt="" />
      </div>
      <div className="global-error__content">
        <span className="global-error__header">Ошибка!</span>
        <span className="global-error__text">{errorText}</span>
      </div>

      <div
        className="global-error__close-btn"
        onClick={() => dispatch(setError(''))}
      >
        Закрыть
      </div>
    </div>
  )
}
