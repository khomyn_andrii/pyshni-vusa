/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef, useState } from 'react'
import { useMediaQuery } from 'react-responsive'
import { useLocation } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'
import arrow from '../../img/arrowDown.svg'
import './Menu.scss'
import { ProductContainer } from './ProductContainer'

function useQuery() {
  return new URLSearchParams(useLocation().search)
}

export const Menu = () => {
  const { categories } = useAppSelector((state) => state.categories)
  const [scrolled, setScrolled] = useState(false)
  const [activeId, setActiveId] = useState(0)
  const [scrollPosition, setScrollPosition] = useState<
    'start' | 'middle' | 'end'
  >('start')
  const refContainer = useRef<HTMLDivElement>(null)

  const locationUrl = useLocation()
  const query = useQuery()

  const isMobile = useMediaQuery({
    query: '(max-width: 768px)',
  })

  const percentObj: { [key: number]: number } = {}
  const fraction = 100 / 3
  let start = 0 - fraction
  if (categories && categories.length && categories[0]._id) {
    categories.forEach((_, idx) => {
      percentObj[idx] = start += fraction
    })
  }

  const handleScroll = () => {
    const offset = window.scrollY
    let neededOffset = 0
    if (isMobile) {
      neededOffset = 0
      if (locationUrl.pathname === '/') {
        neededOffset = 150
      }
    } else if (locationUrl.pathname === '/') {
      neededOffset = 500
    } else {
      neededOffset = 70
    }

    if (offset > neededOffset) {
      setScrolled(true)
    } else {
      setScrolled(false)
    }
  }

  const handleChooserScroll = () => {
    if (refContainer && refContainer.current) {
      let maxWidth =
        refContainer.current.scrollWidth - refContainer.current.clientWidth
      let currPos = refContainer?.current?.scrollLeft
      if (maxWidth - 10 <= currPos) {
        setScrollPosition('end')
      } else if (currPos <= 10) {
        setScrollPosition('start')
      } else {
        setScrollPosition('middle')
      }
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [])

  useEffect(() => {
    refContainer.current?.addEventListener('scroll', handleChooserScroll)
    return () => {
      refContainer.current?.removeEventListener('scroll', handleChooserScroll)
    }
  }, [refContainer.current, locationUrl.pathname])

  useEffect(() => {
    let pos = query.get('pos')
    if (pos) {
      setActiveId(+pos)
    }
  }, [locationUrl.pathname])

  if (!categories) {
    return null
  }

  let categoriesClone = categories
    .slice()
    .sort((a, b) => a.position - b.position)

  if (!isMobile && !locationUrl.pathname.startsWith('/menu')) {
    categoriesClone = categoriesClone.slice(0, 3)
  }

  const itemWidth = isMobile ? '130px' : fraction + '%'

  if (!categoriesClone || !categoriesClone[activeId]) {
    return null
  }

  return (
    <div className="menu">
      {isMobile || !locationUrl.pathname.startsWith('/menu') ? (
        <>
          <div className={'menu__background' + (scrolled ? ' scrolled' : '')}>
            <div
              className={'menu__chooser'}
              ref={refContainer}
              style={{
                width: !isMobile
                  ? 20 * categories.slice(0, 3).length + 'vw'
                  : '100vw',
              }}
            >
              {categoriesClone &&
                categoriesClone.map((category, idx) => (
                  <div
                    className={
                      'menu__chooser__item' +
                      (idx === activeId ? ' active' : '')
                    }
                    key={category._id}
                    style={{
                      minWidth: itemWidth,
                    }}
                    onClick={() => {
                      setActiveId(idx)
                      if (refContainer.current) {
                        refContainer.current.scrollLeft = idx * 130 - 50
                      }
                    }}
                  >
                    {category.title}
                  </div>
                ))}
              <div className="menu__chooser__blank-space" />

              <div
                className="menu__chooser__lever"
                style={{
                  left: percentObj[activeId] + '%',
                  width: itemWidth,
                }}
              />
            </div>
          </div>
          {scrolled && <div className="menu__chooser__empty-space"></div>}
        </>
      ) : (
        <>
          <div className={'menu__background' + (scrolled ? ' scrolled' : '')}>
            <div className={'menu__arrow-chooser'}>
              <div
                className={
                  'menu__arrow-chooser__arrow left' +
                  (scrollPosition === 'start' ? ' disabled' : '')
                }
                onClick={() => {
                  if (refContainer.current) {
                    const scrollLen = refContainer.current.scrollWidth
                    const oneSroll = scrollLen / categories.length
                    refContainer.current.scrollLeft -= oneSroll
                  }
                }}
              >
                <img src={arrow} alt="" />
              </div>
              <div className="menu__arrow-chooser__content" ref={refContainer}>
                {categoriesClone.map((c, idx) => (
                  <div
                    key={idx}
                    className={
                      'menu__arrow-chooser__item' +
                      (idx === activeId ? ' active' : '')
                    }
                    onClick={() => setActiveId(idx)}
                  >
                    {c.title}
                  </div>
                ))}

                <div className="menu__arrow-chooser__empty" />
              </div>
              <div
                className={
                  'menu__arrow-chooser__arrow right' +
                  (scrollPosition === 'end' ? ' disabled' : '')
                }
                onClick={() => {
                  if (refContainer.current) {
                    const scrollLen = refContainer.current.scrollWidth
                    const oneSroll = scrollLen / categories.length
                    refContainer.current.scrollLeft += oneSroll
                  }
                }}
              >
                <img src={arrow} alt="" />
              </div>
            </div>
          </div>
          {scrolled && <div className="menu__empty-space"></div>}
        </>
      )}

      <ProductContainer
        categoryId={categoriesClone[activeId]._id}
        activeId={activeId}
      />
    </div>
  )
}
