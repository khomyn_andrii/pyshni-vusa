import React from 'react'
import { useLocation } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'
import { Card } from '../Card'
import { GoMenuCard } from '../Card/GoMenuCard'
import './ProductContainer.scss'

interface Props {
  categoryId: string
  activeId: number
}

export const ProductContainer = ({ categoryId, activeId }: Props) => {
  const location = useLocation()

  const { products } = useAppSelector((state) => state.products)
  let choosenProducts =
    products && products.filter((p) => p.category === categoryId && p.available)

  if (location.pathname === '/') {
    choosenProducts = choosenProducts.slice(0, 7)
  }

  return (
    <div className="product-container">
      {choosenProducts.length &&
        choosenProducts.map((product) => (
          <Card product={product} key={product._id} />
        ))}
      {location.pathname === '/' && choosenProducts.length === 7 && (
        <GoMenuCard activeId={activeId} />
      )}
    </div>
  )
}
