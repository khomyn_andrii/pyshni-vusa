import React, { useEffect, useState } from 'react'
import cart from '../../img/cart.svg'
import clock from '../../img/clock.svg'
import location from '../../img/location.svg'
import phone from '../../img/phone.svg'
import phone2 from '../../img/phone2.svg'
import phoneWhite from '../../img/phone-white.svg'
import sales from '../../img/sales.svg'
import truck from '../../img/truck.svg'
import logo from '../../img/logo.svg'
import logoNoText from '../../img/logoNoText.svg'
import menuBook from '../../img/menu-book.svg'
import { NavLink, useHistory, useLocation } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'
import { Dropdown } from '../Dropdown'
import { useMediaQuery } from 'react-responsive'
import './Navbar.scss'

interface Props {
  isOpen: boolean
  setIsOpen: React.Dispatch<React.SetStateAction<boolean>>
}

export const Navbar = ({ isOpen, setIsOpen }: Props) => {
  const urlLocation = useLocation()
  const { texts } = useAppSelector((state) => state.texts)
  const [scrolled, setScrolled] = useState(false)
  const [showMenu, setShowMenu] = useState(false)
  const { addedProducts } = useAppSelector((state) => state.cart)
  const history = useHistory()

  const isMobile = useMediaQuery({
    query: '(max-width: 768px)',
  })

  const handleScroll = () => {
    const offset = window.scrollY
    let neededOffset = isMobile ? 0 : 70

    if (offset >= neededOffset) {
      setScrolled(true)
    } else {
      setScrolled(false)
    }
  }

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)
    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const cartPrice = addedProducts.reduce(
    (sum, curr) =>
      (sum +=
        curr.quantity *
        (Number(curr.choosenSize.price) +
          (curr.additions.length &&
            curr.additions.reduce(
              (sum, curr) => (sum += curr.quantity * curr.price),
              0
            )))),
    0
  )

  const cartCount = addedProducts.reduce(
    (acc, curr) => (acc += curr.quantity),
    0
  )

  return (
    <>
      <div className="first-navbar">
        <div className="first-navbar__wrapper">
          <div className="first-navbar__logo">
            <img
              className="first-navbar__logo"
              src={logo}
              alt=""
              onClick={() => history.push('/')}
            />
          </div>
          <div className="first-navbar__phone">
            <img src={phone2} alt="" />
            <a href={`tel:${texts.firstPhone}`}>{texts.firstPhone}</a>
          </div>
          {texts.secondPhone && (
            <div className="first-navbar__phone">
              <img src={phone2} alt="" />
              <a href={`tel:${texts.secondPhone}`}>{texts.secondPhone}</a>
            </div>
          )}

          <div className="first-navbar__time">
            <img src={clock} alt="" />
            <p>
              {texts.startWorkTime} - {texts.endWorkTime}
            </p>
          </div>
          <div className="first-navbar__location">
            <img src={location} alt="" />
            <p>{texts.address}</p>
          </div>
        </div>
      </div>

      <div className={'second-navbar' + (scrolled ? ' scrolled' : '')}>
        <div className="second-navbar__wrapper">
          <div
            className="second-navbar__hidden-logo"
            onClick={() => {
              setIsOpen(false)
              history.push('/')
            }}
          >
            <img src={logoNoText} alt="" />
          </div>
          <NavLink to="/menu" className="second-navbar__item">
            <img src={menuBook} alt="" />
            <p>Меню</p>
          </NavLink>
          <NavLink to="/promotions" className="second-navbar__item percent">
            <img src={sales} alt="" />
            <p>Акції</p>
          </NavLink>
          <NavLink to="/delivery" className="second-navbar__item truck">
            <img src={truck} alt="" />
            <p>Доставка</p>
          </NavLink>
          <NavLink to="/contacts" className="second-navbar__item">
            <img src={phone} alt="" />
            <p>Контакти</p>
          </NavLink>
          <div className={'second-navbar__cart' + (isOpen ? ' hidden' : '')}>
            <div
              className="second-navbar__cart__content"
              onClick={() => {
                if (addedProducts.length) {
                  if (!(urlLocation.pathname === '/order')) {
                    if (isMobile) {
                      return history.push('/order')
                    }
                    setShowMenu(!showMenu)
                  }
                }
              }}
            >
              <p>{Number(cartPrice.toFixed(2))} ГРН</p>
              <div className="second-navbar__cart__content__wrapper">
                {cartCount > 0 ? <p>{cartCount}</p> : <img src={cart} alt="" />}
              </div>
            </div>
            <Dropdown showMenu={showMenu} setShowMenu={setShowMenu} />
          </div>
          <div
            className={'second-navbar__phone' + (isOpen ? ' shown' : '')}
            onClick={() => window.open('tel:+380988979099')}
          >
            <img src={phoneWhite} alt="" />
            <p>+38 098 897 9099</p>
          </div>
          <div
            className={'second-navbar__burger' + (isOpen ? ' active' : '')}
            onClick={() => setIsOpen(!isOpen)}
          >
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
      </div>
      <div className="empty-space" />
    </>
  )
}
