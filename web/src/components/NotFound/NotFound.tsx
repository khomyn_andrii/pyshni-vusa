import React from 'react'
import notFound from '../../img/notFound.svg'
import './NotFound.scss'

interface Props {
  text: string
}

export const NotFound = ({ text }: Props) => {
  return (
    <div className="not-found">
      <div className="not-found__img">
        <img src={notFound} alt="" />
      </div>
      <div className="not-found__text">
        <p>Ooooops!</p>
        <p>{text}</p>
      </div>
    </div>
  )
}
