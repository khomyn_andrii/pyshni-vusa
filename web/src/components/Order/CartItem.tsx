import React from 'react'
import {
  removeOneByKey,
  addOneByKey,
  removeProductByKey,
} from '../../store/cart'
import { useAppDispatch } from '../../store/hooks'
import { ICartProduct } from '../../utils/types'
import closeBtn from '../../img/close-btn.svg'
import { calculateItemPrice } from '../../utils/calculateItemPrice'
import noImage from '../../utils/noImage'

interface ItemProps {
  item: ICartProduct
}

export const CartItem = ({ item }: ItemProps) => {
  const dispatch = useAppDispatch()

  return (
    <div className="order__content__info__item">
      <div
        className="order__content__info__item__img"
        style={{
          backgroundImage: `url(${item.product.image}), url(${noImage})`,
        }}
      />
      <div className="order__content__info__item__wrapper">
        <div className="order__content__info__item__title">
          {item.product.title}
        </div>
        <div className="order__content__info__item__size">
          {item.choosenSize.title}
          {item.choosenSize.diameter > 0
            ? ` - ${item.choosenSize.diameter}`
            : ''}
        </div>
        <div className="order__content__info__item__description">
          {item.product.description}
        </div>
        {item.additions.length > 0 && (
          <div className="order__content__info__item__description">
            Дополнительно: <br />
            {item.additions.map((addition) => (
              <div key={addition._id}>
                {addition.title} x{addition.quantity}
              </div>
            ))}
          </div>
        )}
        <div className="order__content__info__item__price-count">
          <div className="order__content__info__item__price">
            {calculateItemPrice(item)}
            грн
          </div>
          <div className="order__content__info__item__count">
            <button onClick={() => dispatch(removeOneByKey({ key: item.key }))}>
              <span className="minus">_</span>
            </button>{' '}
            {item.quantity}{' '}
            <button onClick={() => dispatch(addOneByKey({ key: item.key }))}>
              <span>+</span>
            </button>
          </div>
        </div>
      </div>
      <div
        className="order__content__info__item__close-btn"
        onClick={() => dispatch(removeProductByKey({ key: item.key }))}
      >
        <img src={closeBtn} alt="" />
      </div>
    </div>
  )
}
