import { Field } from 'formik'
import React from 'react'

interface MyFieldProps {
  name: string
  label: string
}

export const MyField = ({
  name,
  label,
  placeholder,
  type,
  required,
  disabled,
  pattern,
}: MyFieldProps & React.HTMLProps<HTMLInputElement>) => {
  return (
    <div className="order__content__form__row__custom-input">
      <Field
        required={required || false}
        disabled={disabled || false}
        name={name}
        id={name}
        pattern={pattern}
        type={type || 'text'}
        placeholder={placeholder}
      />
      <label htmlFor={name}>{label}</label>
    </div>
  )
}
