import React, { useState } from 'react'
import { useAppDispatch, useAppSelector } from '../../store/hooks'
import { CartItem } from './CartItem'
import { OrderPopup } from './OrderPopup'
import { FormProps, OrderForm } from './OrderForm'
import AxiosService from '../../utils/axiosService'
import { FormikHelpers } from 'formik'
import { IOrder } from '../../utils/types'
import { endLoad, startLoad } from '../../store/loading'
import { clearCart } from '../../store/cart'
import { setError } from '../../store/error'
import moment from 'moment'
import 'moment/locale/ru'
import './Order.scss'

const constructAddress = (
  street: string,
  house: string,
  flat: string,
  entrance: string,
  floor: string
) => {
  const streetStr = 'ул. ' + street + ' ' + house
  const flatStr = ', кв. ' + flat
  const entranceStr = entrance.trim() !== '' ? ', подъезд ' + entrance : ''
  const floorStr = floor.trim() !== '' ? ', этаж ' + floor : ''
  return `${streetStr}${flatStr}${entranceStr}${floorStr}`
}

export const Order = () => {
  const dispatch = useAppDispatch()
  const [orderRes, setOrderRes] = useState<IOrder | null>(null)
  const [tab, setTab] = useState(0)
  const [showPopup, setShowPopup] = useState(false)
  const { addedProducts } = useAppSelector((state) => state.cart)
  const { texts } = useAppSelector((state) => state.texts)

  const cartPrice = addedProducts.reduce(
    (sum, curr) =>
      (sum +=
        curr.quantity *
        (Number(curr.choosenSize.price) +
          (curr.additions.length &&
            curr.additions.reduce(
              (sum, curr) => (sum += curr.quantity * curr.price),
              0
            )))),
    0
  )

  const handleSubmit = (
    values: FormProps,
    { resetForm }: FormikHelpers<FormProps>
  ) => {
    const service = new AxiosService()
    console.log(
      moment(values.deliveryTime, 'HH:mm').diff(moment.now(), 'minutes')
    )
    if (addedProducts.length === 0) {
      return dispatch(setError('Корзина не должна быть пустой'))
    }

    if (
      !values.deliveryDate ||
      values.deliveryDate === 'Дата' ||
      !values.deliveryTime ||
      values.deliveryTime === 'Время'
    ) {
      return dispatch(setError('Не забудьте выбрать время'))
    }

    if (
      values.deliveryDate === 'Сегодня' &&
      moment(values.deliveryTime, 'HH:mm').diff(moment.now(), 'minutes') <
        texts.minOrderTime
    ) {
      return dispatch(setError('Выберите более позднее время'))
    }

    setTimeout(async () => {
      dispatch(startLoad())

      const deliveryDate =
        values.deliveryDate === 'Сегодня'
          ? moment().format('ddd DD/MM')
          : values.deliveryDate
      const date = deliveryDate + values.deliveryTime
      const formattedDate = moment(date, 'ddd DD/MM HH:mm').toDate()
      const sum = cartPrice.toFixed(2)
      const deliveryType = tab === 0 ? 'delivery' : 'pickup'
      const { street, house, flat, entrance, floor } = values
      const address = constructAddress(street, house, flat, entrance, floor)

      const newOrder = {
        ...values,
        deliveryType,
        address,
        sum,
        products: addedProducts,
        date: formattedDate,
        status: 'new',
      }

      try {
        dispatch(startLoad())
        const res = await service.post('/order', newOrder, true)
        setOrderRes(res)
        resetForm()
        dispatch(clearCart())
        setShowPopup(true)
        window.dataLayer.push({'event': 'Order'})
      } catch (err) {
        dispatch(setError('Что-то пошло не так. Попробуйте еще раз позже'))
        console.log(err)
      } finally {
        setTimeout(() => dispatch(endLoad()), 500)
      }
    }, 500)
  }

  return (
    <div className="order">
      {showPopup ? (
        <OrderPopup setShowPopup={setShowPopup} orderRes={orderRes} />
      ) : null}
      <div className="order__content">
        <div className="order__content--left">
          <div className="order__content__header">Оформление заказа</div>
          <div className="order__content__toggle">
            <div
              className={
                'order__content__toggle__item' + (tab === 0 ? ' active' : '')
              }
              onClick={() => setTab(0)}
            >
              Доставка курьером
            </div>
            <div
              className={
                'order__content__toggle__item' + (tab === 1 ? ' active' : '')
              }
              onClick={() => setTab(1)}
            >
              Забрать самому
            </div>
            <div
              className="order__content__toggle__lever"
              style={{ left: `${tab * 50}%` }}
            ></div>
          </div>
          <OrderForm
            tab={tab}
            handleSubmit={handleSubmit}
            minOrderTime={texts.minOrderTime}
          />
        </div>
        <div className="order__content--right">
          <div className="order__content__header">Ваш заказ</div>
          <div className="order__content__info">
            {addedProducts.length ? (
              <>
                {addedProducts.map((item, i) => (
                  <CartItem item={item} key={i} />
                ))}
                <div className="order__content__info__price">
                  {Number(cartPrice.toFixed(2))} грн
                </div>
              </>
            ) : (
              <div className="order__content__info__empty">
                В корзине пусто!
              </div>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}
