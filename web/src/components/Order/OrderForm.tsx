import { Formik, FormikProps, Form, Field, FormikHelpers } from 'formik'
import { capitalize } from 'lodash'
import moment from 'moment'
import React from 'react'
import Toggle from 'react-toggle'
import { MyField } from './MyField'
import 'react-toggle/style.css'

interface Props {
  tab: number
  minOrderTime: number
  handleSubmit: (values: FormProps, helpers: FormikHelpers<FormProps>) => void
}

export interface FormProps {
  customerName: string
  customerPhone: string
  customerEmail: string
  street: string
  house: string
  flat: string
  entrance: string
  floor: string
  comment: string
  changeFrom: string
  paymentType: 'card' | 'cash'
  deliveryTime: string
  deliveryDate: string
  callback: boolean
}

export const OrderForm = ({ tab, handleSubmit, minOrderTime }: Props) => {
  moment.locale('ru')
  const nextDay = capitalize(moment().add(1, 'day').format('ddd DD/MM '))
  const nextNextDay = capitalize(moment().add(2, 'day').format('ddd DD/MM '))

  const genTimeStamps = () => {
    const startOfDay = moment().startOf('day')
    const timestamps = [startOfDay.clone()]
    const numberOfStamps = moment.duration(1, 'day').as('m') / 10
    for (let index = 1; index < numberOfStamps; index++) {
      startOfDay.add(10, 'minutes')
      timestamps.push(startOfDay.clone())
    }
    return timestamps
  }

  const timestamps = genTimeStamps()

  return (
    <Formik
      initialValues={{
        customerName: '',
        customerPhone: '',
        customerEmail: '',
        street: '',
        house: '',
        flat: '',
        entrance: '',
        floor: '',
        comment: '',
        changeFrom: '',
        paymentType: 'cash',
        deliveryDate: '',
        deliveryTime: '',
        callback: true,
      }}
      onSubmit={handleSubmit}
    >
      {({ values, setFieldValue }: FormikProps<FormProps>) => (
        <Form className="order__content__form">
          <div className="order__content__form__header">Контакти</div>
          <div className="order__content__form__row">
            <MyField required name="customerName" label="Имя*" />
            <MyField
              required
              type="tel"
              name="customerPhone"
              placeholder="+380501112222"
              label="Телефон*"
              pattern="^\+?380\d{3}\d{2}\d{2}\d{2}$"
            />
            <MyField name="customerEmail" type="email" label="E-mail" />
          </div>

          {tab === 0 ? (
            <>
              <div className="order__content__form__header">Адрес доставки</div>
              <div className="order__content__form__row smaller">
                <MyField required name="street" label="Улица*" />
                <MyField required name="house" label="Дом*" />
                <MyField required name="flat" label="Квартира*" />
                <MyField name="entrance" label="Подъезд" />
                <MyField name="floor" label="Этаж" />
              </div>
            </>
          ) : (
            <>
              <div className="order__content__form__header">Адрес</div>
              <div className="order__content__form__row">
                <input
                  disabled
                  value="пр. Правды, 41-Г"
                  style={{ marginBottom: '20px' }}
                />
              </div>
            </>
          )}
          <Field
            as="textarea"
            name="comment"
            rows={4}
            placeholder="Комментарий"
            className="order__content__form__comment"
          ></Field>
          <div className="order__content__form__header">
            Доставка по времени
          </div>
          <div className="order__content__form__row">
            <Field
              as="select"
              name="deliveryDate"
              onChange={(e: React.ChangeEvent<HTMLSelectElement>) => {
                setFieldValue('deliveryDate', e.target.value)
                setFieldValue('deliveryTime', 'Время')
              }}
            >
              <option hidden>Дата</option>
              <option value={'Сегодня'}>Сегодня</option>
              <option value={nextDay}>{nextDay}</option>
              <option value={nextNextDay}>{nextNextDay}</option>
            </Field>
            <Field as="select" name="deliveryTime">
              <option hidden>Время</option>
              {timestamps.map((t) => {
                let time = t.format('HH:mm')

                const { deliveryDate } = values
                let disabled = false

                if (deliveryDate === '') {
                  disabled = false
                }

                if (deliveryDate === 'Сегодня') {
                  const now = moment()
                  if (
                    now.add(minOrderTime - 1, 'minutes').diff(t, 'minutes') > 0
                  ) {
                    disabled = true
                  }
                }

                return (
                  <option hidden={disabled} value={time} key={time}>
                    {time}
                  </option>
                )
              })}
            </Field>
          </div>
          <div className="order__content__form__header">Оплата</div>
          <div className="order__content__form__row no-margin">
            <Field as={'select'} name="paymentType">
              <option value="card">Картой</option>
              <option value="cash">Наличными</option>
            </Field>
            <MyField
              required={false}
              disabled={values.paymentType === 'card'}
              name="changeFrom"
              label="Сдача с"
            />
          </div>
          <label className="order__content__callback">
            <Toggle
              defaultChecked={values.callback}
              icons={false}
              onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                setFieldValue('callback', e.target.checked)
              }
            />
            <span>Перезвонить для уточнения заказа</span>
          </label>
          <div className="order__content__form__btn">
            <button type="submit">Заказать</button>
          </div>
        </Form>
      )}
    </Formik>
  )
}
