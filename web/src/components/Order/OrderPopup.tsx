import moment from 'moment'
import React from 'react'
import { useMediaQuery } from 'react-responsive'
import closeButton from '../../img/close-btn.svg'
import { calculateCartPrice } from '../../utils/calculateCartPrice'
import { calculateItemPrice } from '../../utils/calculateItemPrice'
import { IOrder } from '../../utils/types'

interface Props {
  setShowPopup: React.Dispatch<React.SetStateAction<boolean>>
  orderRes: IOrder | null
}

const ProductList = ({ orderRes }: { orderRes: IOrder }) => {
  return (
    <>
      {orderRes.products.map((item) => (
        <div className="order__popup__grid__product" key={item.choosenSize._id}>
          <div className="order__popup__grid__product__title">
            {item.quantity}х {item.product.title}
          </div>
          <div className="order__popup__grid__product__size">
            {item.choosenSize.title}{' '}
            {item.choosenSize.diameter > 0
              ? ` - ${item.choosenSize.diameter}`
              : ''}
          </div>
          {item.additions.length > 0 && (
            <div className="order__popup__grid__product__description">
              Дополнительно:
              {item.additions.map((a) => (
                <div key={a._id}>
                  {a.title} x{a.quantity}
                </div>
              ))}
            </div>
          )}
          <div className="order__popup__grid__product__price">
            {calculateItemPrice(item)} грн
          </div>
        </div>
      ))}
    </>
  )
}

export const OrderPopup = ({ setShowPopup, orderRes }: Props) => {
  const isMobile = useMediaQuery({
    query: '(max-width: 768px)',
  })

  if (!orderRes) {
    return null
  }

  const cartPrice = calculateCartPrice(orderRes)

  return (
    <div className="order__popup">
      <div className="order__popup__content">
        <img
          src={closeButton}
          className="order__popup__close-btn"
          alt=""
          onClick={() => setShowPopup(false)}
        />
        <div className="order__popup__header">Спасибо за Ваш заказ!</div>
        <div className="order__popup__grid">
          {!isMobile ? (
            <>
              <div className="order__popup__grid__item left">
                <div className="order__popup__grid__order-num">
                  #{orderRes.key}
                </div>
                <div className="order__popup__grid__order-details">
                  <p>
                    <strong>Доставка: </strong>{' '}
                    {orderRes.deliveryType === 'delivery'
                      ? 'курьером'
                      : 'самовывоз'}
                  </p>
                  {orderRes.deliveryType === 'delivery' && (
                    <p>
                      <strong>Адрес доставки: </strong> {orderRes.address}
                    </p>
                  )}
                  <p>
                    <strong>Время доставки: </strong>{' '}
                    {moment(orderRes.date).format('ddd DD/MM HH:mm')}
                  </p>
                  <p>
                    <strong>Оплата: </strong>{' '}
                    {orderRes.paymentType === 'card' ? 'картой' : 'наличкой'}
                  </p>
                </div>
              </div>
              <div className="order__popup__grid__item right">
                <ProductList orderRes={orderRes} />

                <div className="order__popup__grid__total-price">
                  Итого: {Number(cartPrice.toFixed(2))} грн
                </div>
              </div>
            </>
          ) : (
            <>
              <div className="order__popup__grid__item left">
                <div className="order__popup__grid__order-num">
                  #{orderRes.key}
                </div>
              </div>
              <div className="order__popup__grid__item right">
                <ProductList orderRes={orderRes} />
                <div className="order__popup__grid__total-price">
                  Итого: {Number(cartPrice.toFixed(2))} грн
                </div>
              </div>
              <div className="order__popup__grid__order-details">
                <p>
                  <strong>Доставка: </strong>{' '}
                  {orderRes.deliveryType === 'delivery'
                    ? 'курьером'
                    : 'самовывоз'}
                </p>
                {orderRes.deliveryType === 'delivery' && (
                  <p>
                    <strong>Адрес доставки: </strong> {orderRes.address}
                  </p>
                )}
                <p>
                  <strong>Время доставки: </strong>{' '}
                  {moment(orderRes.date).format('ddd DD/MM HH:mm')}
                </p>
                <p>
                  <strong>Оплата: </strong>{' '}
                  {orderRes.paymentType === 'card' ? 'картой' : 'наличкой'}
                </p>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  )
}
