import React from 'react'
import { IAdditionCategory } from '../../utils/types'

interface Props {
  category: IAdditionCategory
  updateItemQuantity: (
    categoryId: string,
    additionId: string,
    quantity: number
  ) => void
}

export const AdditionCategory = ({ category, updateItemQuantity }: Props) => {
  if (!category) {
    return null
  }

  return (
    <div className="product-view__addition-category">
      {category.additions.map((addition) => (
        <div className="product-view__addition-item" key={addition._id}>
          <div
            className="product-view__addition-item__img"
            style={{ backgroundImage: `url(${addition.image})` }}
          ></div>
          <div className="product-view__addition-item__field">
            {addition.title}
          </div>
          <div className="product-view__addition-item__field">
            {addition.weight} гр
          </div>
          <div className="product-view__addition-item__field">
            {addition.price} грн
          </div>
          <div className="product-view__addition-item__btns">
            <div
              className="product-view__addition-item__btns__btn"
              onClick={() => {
                if (addition.quantity !== 0) {
                  updateItemQuantity(
                    category._id,
                    addition._id,
                    addition.quantity - 1
                  )
                }
              }}
            >
              <span>_</span>
            </div>
            <span className="product-view__addition-item__btns__text">
              {addition.quantity}
            </span>
            <div
              className="product-view__addition-item__btns__btn"
              onClick={() => {
                updateItemQuantity(
                  category._id,
                  addition._id,
                  addition.quantity + 1
                )
              }}
            >
              <span>+</span>
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}
