import React, { useState } from 'react'
import { useHistory } from 'react-router'
import closeBtn from '../../img/close-btn.svg'
import { addProduct } from '../../store/cart'
import { useAppDispatch } from '../../store/hooks'
import { IAddition, IProduct, ISize } from '../../utils/types'
import './ProductPage.scss'

interface Props {
  modalOpen: boolean
  setModalOpen: React.Dispatch<React.SetStateAction<boolean>>
  choosen: IAddition[]
  product: IProduct
  size: ISize
}

export const ProductModal = ({
  modalOpen,
  setModalOpen,
  choosen,
  product,
  size,
}: Props) => {
  const dispatch = useAppDispatch()
  const history = useHistory()
  const [quantity, setQuantity] = useState(1)
  const refBtn = React.createRef<HTMLButtonElement>()

  if (!modalOpen) {
    return null
  }

  const additionsPrice = choosen.reduce(
    (sum, a) => sum + a.price * a.quantity,
    0
  )

  return (
    <div className="product-modal">
      <div className="product-modal__content">
        <div
          className="product-modal__close-btn"
          onClick={() => setModalOpen(false)}
        >
          <img src={closeBtn} alt="" />
        </div>
        <div className="product-modal__title">{product.title}</div>
        <div className="product-modal__ingredients">
          <div className="product-modal__ingredients__column">
            {choosen.length > 0 &&
              choosen.map((a) => (
                <span key={a._id}>
                  {a.title} x{a.quantity}
                </span>
              ))}
          </div>
        </div>
        <div className="product-modal__actions-btns">
          <button
            onClick={() => {
              if (quantity > 1) {
                setQuantity((quantity) => quantity - 1)
              }
            }}
          >
            <span className="minus">_</span>
          </button>
          <span className="count">{quantity}</span>
          <button onClick={() => setQuantity((quantity) => quantity + 1)}>
            <span>+</span>
          </button>
        </div>
        <div className="product-modal__bottom">
          <div className="product-modal__price">
            {(quantity * (Number(size.price) + Number(additionsPrice))).toFixed(
              2
            )}{' '}
            грн
          </div>
          <button
            ref={refBtn}
            className="product-modal__cart-btn"
            onClick={() => {
              dispatch(
                addProduct({
                  product,
                  quantity,
                  choosenSize: size,
                  additions: choosen,
                })
              )
              window.dataLayer.push({ event: 'AddToCart' })
              if (refBtn.current) {
                refBtn.current.textContent = 'Добавлено ✓'
              }
              setTimeout(() => history.push('/menu'), 500)
            }}
          >
            В корзину
          </button>
        </div>
      </div>
    </div>
  )
}
