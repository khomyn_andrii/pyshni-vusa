import React, { useState } from 'react'
import { useHistory, useParams } from 'react-router'
import { useAppSelector } from '../../store/hooks'
import { AdditionCategory } from './AdditionCategory'
import arrowDown from '../../img/arrowDown.svg'
import './ProductPage.scss'
import { IAdditionCategory } from '../../utils/types'
import { ProductModal } from './ProductModal'
import * as _ from 'lodash'
import { Redirect } from 'react-router-dom'
import noImage from '../../utils/noImage'

interface URLParams {
  id: string
}

interface Props {
  additionCategories: IAdditionCategory[]
}

export const ProductPage = ({ additionCategories }: Props) => {
  const history = useHistory()
  const { products } = useAppSelector((state) => state.products)
  const { categories } = useAppSelector((state) => state.categories)
  const [additions, setAdditions] = useState<IAdditionCategory[]>(
    additionCategories
  )
  const [activeSize, setActiveSize] = useState(0)
  const [activeAdditions, setActiveAdditions] = useState(0)
  const [modalOpen, setModalOpen] = useState(false)
  let { id } = useParams<URLParams>()

  const product = products.find((p) => p._id === id)

  const updateItemQuantity = (
    categoryId: string,
    additionId: string,
    quantity: number
  ) => {
    if (additions) {
      const categToUpdate = additions.find((c) => c._id === categoryId)

      if (categToUpdate) {
        const updAdditions = categToUpdate.additions.map((a) =>
          a._id === additionId ? { ...a, quantity: quantity } : a
        )
        setAdditions(
          additions.map((c) =>
            c._id === categoryId
              ? {
                  ...categToUpdate,
                  additions: updAdditions,
                }
              : c
          )
        )
      }
    }
  }

  if (!product) {
    return <Redirect to="/404" />
  }

  if (!product || !additions) {
    return null
  }

  const category = categories.find((c) => c._id === product?.category)

  const sizes = product.sizes.slice().sort((a, b) => a.diameter - b.diameter)
  const categoriesFraction = 100 / sizes.length
  const additionsFraction = 100 / additionCategories.length
  const choosenAdditions = _.flatten(
    additions.map((a) => a.additions.filter((a) => a.quantity > 0))
  )
  const additionsPrice = choosenAdditions.reduce(
    (sum, a) => sum + a.price * a.quantity,
    0
  )

  return (
    <>
      <ProductModal
        modalOpen={modalOpen}
        setModalOpen={setModalOpen}
        product={product}
        size={sizes[activeSize]}
        choosen={choosenAdditions}
      />
      <div className="product-view">
        <div
          className="product-view__back-btn"
          onClick={() => history.goBack()}
        >
          <span>
            <img src={arrowDown} alt="" /> Назад в меню
          </span>
        </div>
        <div className="product-view__grid">
          <div
            className="product-view__img"
            style={{
              backgroundImage: `url(${product.image}), url(${noImage})`,
            }}
          />
          <div className="product-view__details">
            <div className="product-view__title">{product.title}</div>
            <div className="product-view__chooser">
              {sizes.map((size, idx) => (
                <div
                  className={
                    'product-view__chooser__size' +
                    (activeSize === idx ? ' active' : '')
                  }
                  style={{ width: categoriesFraction + '%' }}
                  key={size._id}
                  onClick={() => setActiveSize(idx)}
                >
                  {size.title} {size.diameter > 0 ? `- ${size.diameter}` : ''}
                </div>
              ))}
              <div
                className="product-view__chooser__lever"
                style={{
                  width: categoriesFraction + '%',
                  left: categoriesFraction * activeSize + '%',
                }}
              ></div>
            </div>
            <div className="product-view__description">
              {product.description}
            </div>
            <div className="product-view__price">
              {Number(
                (
                  Number(sizes[activeSize].price) + Number(additionsPrice)
                ).toFixed(2)
              )}{' '}
              грн
            </div>
            <div className="product-view__cart-btns">
              <button
                onClick={() => {
                  setModalOpen(true)
                }}
              >
                В корзину
              </button>
            </div>
            {category && category.showAdditions && (
              <>
                <div className="product-view__header">
                  Добавление ингредиентов
                </div>
                <div className="product-view__chooser">
                  {additions.map((addition, idx) => (
                    <div
                      className={
                        'product-view__chooser__size' +
                        (activeAdditions === idx ? ' active' : '')
                      }
                      style={{ width: additionsFraction + '%' }}
                      key={addition._id}
                      onClick={() => setActiveAdditions(idx)}
                    >
                      {addition.title}
                    </div>
                  ))}
                  <div
                    className="product-view__chooser__lever"
                    style={{
                      width: additionsFraction + '%',
                      left: additionsFraction * activeAdditions + '%',
                    }}
                  ></div>
                </div>
                <AdditionCategory
                  updateItemQuantity={updateItemQuantity}
                  category={additions[activeAdditions]}
                />
              </>
            )}
          </div>
        </div>
      </div>
    </>
  )
}
