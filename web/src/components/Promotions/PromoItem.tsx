import React, { useEffect, useRef, useState } from 'react'
import { useHistory } from 'react-router-dom'
import noImage from '../../utils/noImage'
import { IPromotion } from '../../utils/types'

interface Props {
  promotion: IPromotion
}

export const PromoItem = ({ promotion }: Props) => {
  const history = useHistory()
  const ref = useRef<HTMLParagraphElement>(null)
  const [cropped, setCropped] = useState(false)

  useEffect(() => {
    if (ref.current && ref.current.offsetHeight / 25 > 4) {
      setCropped(true)
    }
  }, [])

  return (
    <div className="promotions__item" key={promotion._id}>
      <div
        className="promotions__item__img"
        style={{ backgroundImage: `url(${promotion.image}), url(${noImage})` }}
      ></div>
      <div className="promotions__item__text">
        <h2>{promotion.title}</h2>
        <p className={cropped ? 'cropped' : ''} ref={ref}>
          {promotion.description}
        </p>
        {cropped && (
          <button
            onClick={() => {
              history.push('/promotions/' + promotion._id)
            }}
          >
            Подробнее
          </button>
        )}
      </div>
    </div>
  )
}
