import React from 'react'
import { Redirect, useParams } from 'react-router-dom'
import { useAppSelector } from '../../store/hooks'
import noImage from '../../utils/noImage'
import './Promotions.scss'

interface URLParams {
  id: string
}

export const PromotionPage = () => {
  let { id } = useParams<URLParams>()

  const { promotions } = useAppSelector((state) => state.promotions)

  const promotion = promotions.find((p) => p._id === id)

  if (!promotion) {
    return <Redirect to="/404" />
  }

  return (
    <div className="promotions">
      <div className="promotions__item" key={promotion._id}>
        <div
          className="promotions__item__img"
          style={{
            backgroundImage: `url(${promotion.image}), url(${noImage})`,
          }}
        ></div>
        <div className="promotions__item__text">
          <h2>{promotion.title}</h2>
          <p>{promotion.description}</p>
        </div>
      </div>
    </div>
  )
}
