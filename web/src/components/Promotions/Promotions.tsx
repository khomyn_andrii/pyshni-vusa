import React from 'react'
import { useAppSelector } from '../../store/hooks'
import { PromoItem } from './PromoItem'

import './Promotions.scss'

export const Promotions = () => {
  const { promotions } = useAppSelector((state) => state.promotions)

  if (!promotions) {
    return null
  }

  return (
    <div className="promotions">
      <div className="promotions__header">Акції</div>
      {promotions.length > 0 &&
        promotions.map((promotion) => (
          <PromoItem promotion={promotion} key={promotion._id} />
        ))}
    </div>
  )
}
