import React from 'react'
import { hydrate, render } from 'react-dom'
import './index.scss'
import App from './App'
import { Provider } from 'react-redux'
import { store } from './store'
// import { BrowserRouter as Router } from 'react-router-dom'
import { HashRouter as Router } from 'react-router-dom'

const rootElement = document.getElementById('root')
if (rootElement && rootElement.hasChildNodes()) {
  hydrate(
    <React.StrictMode>
      <Provider store={store}>
        <Router>
          <App isHydrate={true} />
        </Router>
      </Provider>
    </React.StrictMode>,
    rootElement
  )
} else {
  render(
    <React.StrictMode>
      <Provider store={store}>
        <Router>
          <App isHydrate={false} />
        </Router>
      </Provider>
    </React.StrictMode>,
    rootElement
  )
}
