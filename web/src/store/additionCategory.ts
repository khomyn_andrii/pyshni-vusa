import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IAddition, IAdditionCategory } from '../utils/types'

// Define a type for the slice state
interface AdditionCategoryState {
  additionCategories: IAdditionCategory[]
}

// Define the initial state using that type
const initialState: AdditionCategoryState = {
  additionCategories: [],
}

const getCategory = (state: AdditionCategoryState, categoryId: string) => {
  return state.additionCategories.find((c) => c._id === categoryId)
}

export const additionCategorySlice = createSlice({
  name: 'additionCategory',
  initialState,
  reducers: {
    setAdditionCategories: (
      state,
      { payload }: PayloadAction<IAdditionCategory[]>
    ) => {
      state.additionCategories = payload.map((c) => ({
        ...c,
        additions: c.additions.map((a) => ({ ...a, quantity: 0 })),
      }))
    },
    removeAdditionCategory: (state, { payload }: PayloadAction<string>) => {
      state.additionCategories = state.additionCategories.filter(
        (c) => c._id !== payload
      )
    },
    addAdditionCategory: (
      state,
      { payload }: PayloadAction<IAdditionCategory>
    ) => {
      state.additionCategories = [
        { ...payload, additions: [] },
        ...state.additionCategories,
      ]
    },
    addItemToCategory: (
      state,
      { payload }: PayloadAction<{ categoryId: string; addition: IAddition }>
    ) => {
      const categToUpdate = getCategory(state, payload.categoryId)
      if (categToUpdate) {
        categToUpdate.additions = [...categToUpdate.additions, payload.addition]
        state.additionCategories = state.additionCategories.map((c) =>
          c._id === payload.categoryId ? categToUpdate : c
        )
      }
    },
    updateItemInCategory: (
      state,
      {
        payload,
      }: PayloadAction<{
        categoryId: string
        addition: IAddition
        idToUpdate: string
      }>
    ) => {
      const categToUpdate = getCategory(state, payload.categoryId)
      if (categToUpdate) {
        categToUpdate.additions = categToUpdate.additions.map((a) =>
          a._id !== payload.idToUpdate ? a : payload.addition
        )
        state.additionCategories = state.additionCategories.map((c) =>
          c._id === payload.categoryId ? categToUpdate : c
        )
      }
    },
    removeItemFromCategory: (
      state,
      { payload }: PayloadAction<{ categoryId: string; addition: IAddition }>
    ) => {
      const categToUpdate = getCategory(state, payload.categoryId)
      if (categToUpdate) {
        categToUpdate.additions = categToUpdate.additions.filter(
          (a) => a._id !== payload.addition._id
        )
        state.additionCategories = state.additionCategories.map((c) =>
          c._id === payload.categoryId ? categToUpdate : c
        )
      }
    },
  },
})

export const {
  setAdditionCategories,
  removeAdditionCategory,
  addAdditionCategory,
  addItemToCategory,
  updateItemInCategory,
  removeItemFromCategory,
} = additionCategorySlice.actions

export default additionCategorySlice.reducer
