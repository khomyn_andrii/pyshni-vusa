import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IAddition, ICartProduct, IProduct, ISize } from '../utils/types'

interface ICartPayload {
  product: IProduct
  choosenSize: ISize
  additions?: IAddition[]
  quantity?: number
}

interface CartState {
  addedProducts: ICartProduct[]
}

const initialState: CartState = {
  addedProducts: [],
}

const removeFromState = (state: CartState, payload: ICartPayload) => {
  const filtered = state.addedProducts.filter(
    (p) =>
      p.choosenSize._id !== payload.choosenSize._id && p.additions.length === 0
  )
  state.addedProducts = filtered
}

const removeFromStateByKey = (state: CartState, payload: { key: number }) => {
  const filtered = state.addedProducts.filter((p) => p.key !== payload.key)
  state.addedProducts = filtered
}

export const cartSlice = createSlice({
  name: 'cart',
  initialState,
  reducers: {
    addProduct: (state, { payload }: PayloadAction<ICartPayload>) => {
      const newProduct = {
        product: payload.product,
        choosenSize: payload.choosenSize,
        quantity: payload.quantity || 1,
        additions: payload.additions || [],
        key: state.addedProducts.length,
      }
      state.addedProducts = [...state.addedProducts, newProduct]
    },
    removeProduct: (state, { payload }: PayloadAction<ICartPayload>) => {
      removeFromState(state, payload)
    },
    removeProductByKey: (
      state,
      { payload }: PayloadAction<{ key: number }>
    ) => {
      removeFromStateByKey(state, payload)
    },
    addOne: (state, { payload }: PayloadAction<ICartPayload>) => {
      const idx = state.addedProducts.findIndex(
        (p) =>
          p.product._id === payload.product._id &&
          p.choosenSize._id === payload.choosenSize._id &&
          p.additions.length === 0
      )
      if (idx >= 0) {
        const updProduct = state.addedProducts[idx]
        updProduct.quantity += 1

        state.addedProducts = state.addedProducts.map((p) =>
          p.key === updProduct.key ? updProduct : p
        )
      }
    },
    addOneByKey: (state, { payload }: PayloadAction<{ key: number }>) => {
      const idx = state.addedProducts.findIndex((p) => p.key === payload.key)
      if (idx >= 0) {
        const updProduct = state.addedProducts[idx]
        updProduct.quantity += 1
        state.addedProducts = state.addedProducts.map((p) =>
          p.key === payload.key ? updProduct : p
        )
      }
    },
    removeOne: (state, { payload }: PayloadAction<ICartPayload>) => {
      const idx = state.addedProducts.findIndex(
        (p) =>
          p.product._id === payload.product._id &&
          p.choosenSize._id === payload.choosenSize._id &&
          p.additions.length === 0
      )
      if (idx >= 0) {
        if (state.addedProducts[idx].quantity === 1) {
          removeFromState(state, payload)
        } else {
          const updProduct = state.addedProducts[idx]
          updProduct.quantity -= 1
          state.addedProducts = state.addedProducts.map((p) =>
            p.key === updProduct.key ? updProduct : p
          )
        }
      }
    },
    removeOneByKey: (state, { payload }: PayloadAction<{ key: number }>) => {
      const idx = state.addedProducts.findIndex((p) => p.key === payload.key)
      if (idx >= 0) {
        if (state.addedProducts[idx].quantity === 1) {
          removeFromStateByKey(state, payload)
        } else {
          const updProduct = state.addedProducts[idx]
          updProduct.quantity -= 1
          state.addedProducts = state.addedProducts.map((p) =>
            p.key === payload.key ? updProduct : p
          )
        }
      }
    },
    clearCart: (state) => {
      state.addedProducts = []
    },
  },
})

export const {
  addProduct,
  addOne,
  removeProduct,
  removeProductByKey,
  removeOne,
  clearCart,
  addOneByKey,
  removeOneByKey,
} = cartSlice.actions

export default cartSlice.reducer
