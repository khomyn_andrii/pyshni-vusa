import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ICategory } from '../utils/types'

// Define a type for the slice state
interface CategoryState {
  categories: ICategory[]
}

// Define the initial state using that type
const initialState: CategoryState = {
  categories: [],
}

export const categorySlice = createSlice({
  name: 'category',
  initialState,
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    setCategories: (state, { payload }: PayloadAction<ICategory[]>) => {
      state.categories = payload
    },
    removeCategory: (state, { payload }: PayloadAction<string>) => {
      state.categories = state.categories.filter((c) => c._id !== payload)
    },
    addCategory: (state, { payload }: PayloadAction<ICategory>) => {
      state.categories = [
        { ...payload, showAdditions: payload.showAdditions || false },
        ...state.categories,
      ]
    },
  },
})

export const {
  setCategories,
  removeCategory,
  addCategory,
} = categorySlice.actions

export default categorySlice.reducer
