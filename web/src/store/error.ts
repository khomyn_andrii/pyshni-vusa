import { createSlice, PayloadAction } from '@reduxjs/toolkit'

// Define a type for the slice state
interface ErrorState {
  errorText: string
}

// Define the initial state using that type
const initialState: ErrorState = {
  errorText: '',
}

export const errorSlice = createSlice({
  name: 'error',
  initialState,
  reducers: {
    setError: (state, { payload }: PayloadAction<string>) => {
      state.errorText = payload
    },
  },
})

export const { setError } = errorSlice.actions

export default errorSlice.reducer
