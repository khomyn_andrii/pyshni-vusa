import { configureStore } from '@reduxjs/toolkit'
import productReducer from './product'
import categoryReducer from './category'
import loadingReducer from './loading'
import promotionReducer from './promotion'
import additionCategoryReducer from './additionCategory'
import modalReducer from './modal'
import cartReducer from './cart'
import errorReducer from './error'
import textsReducer from './texts'

export const store = configureStore({
  reducer: {
    products: productReducer,
    categories: categoryReducer,
    additionCategories: additionCategoryReducer,
    loading: loadingReducer,
    promotions: promotionReducer,
    cart: cartReducer,
    modal: modalReducer,
    error: errorReducer,
    texts: textsReducer,
  },
})

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch
