import { createSlice } from '@reduxjs/toolkit'

// Define a type for the slice state
interface LoadingState {
  loading: boolean
}

// Define the initial state using that type
const initialState: LoadingState = {
  loading: false,
}

export const loadingSlice = createSlice({
  name: 'loading',
  initialState,
  reducers: {
    startLoad: (state) => {
      state.loading = true
    },
    endLoad: (state) => {
      state.loading = false
    },
  },
})

export const { startLoad, endLoad } = loadingSlice.actions

export default loadingSlice.reducer
