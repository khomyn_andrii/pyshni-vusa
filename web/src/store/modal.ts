import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IProduct } from '../utils/types'

interface ModalState {
  isOpen: boolean
  product: IProduct | null
  category: string | null
}

const initialState: ModalState = {
  isOpen: false,
  product: null,
  category: null,
}

export const modalSlice = createSlice({
  name: 'modal',
  initialState,
  reducers: {
    openModal: (state, { payload }: PayloadAction<IProduct | string>) => {
      state.isOpen = true
      if (typeof payload === 'string') {
        state.category = payload
        state.product = null
      } else {
        state.product = payload
        state.category = null
      }
    },
    closeModal: (state) => {
      state.isOpen = false
      state.product = null
      state.category = null
    },
  },
})

export const { openModal, closeModal } = modalSlice.actions

export default modalSlice.reducer
