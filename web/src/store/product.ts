import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IProduct } from '../utils/types'
import { RootState } from './index'

// Define a type for the slice state
interface ProductState {
  products: IProduct[]
}

// Define the initial state using that type
const initialState: ProductState = {
  products: [],
}

export const productSlice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    // Use the PayloadAction type to declare the contents of `action.payload`
    setProducts: (state, { payload }: PayloadAction<IProduct[]>) => {
      state.products = payload
    },
    updateProducts: (state, { payload }: PayloadAction<IProduct>) => {
      state.products = state.products.map((product) =>
        product._id !== payload._id ? product : payload
      )
    },
    addProductToState: (state, { payload }: PayloadAction<IProduct>) => {
      state.products = [...state.products, payload]
    },
    removeProductFromState: (state, { payload }: PayloadAction<IProduct>) => {
      state.products = state.products.filter((p) => p._id !== payload._id)
    },
  },
})

export const {
  setProducts,
  removeProductFromState,
  addProductToState,
  updateProducts,
} = productSlice.actions

// Other code such as selectors can use the imported `RootState` type
export const selectProducts = (state: RootState) => state.products

export default productSlice.reducer
