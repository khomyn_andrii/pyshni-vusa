import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IPromotion } from '../utils/types'

// Define a type for the slice state
interface PromotionState {
  promotions: IPromotion[]
}

// Define the initial state using that type
const initialState: PromotionState = {
  promotions: [],
}

export const promotionSlice = createSlice({
  name: 'promotion',
  initialState,
  reducers: {
    setPromotions: (state, { payload }: PayloadAction<IPromotion[]>) => {
      state.promotions = payload
    },
    removePromotion: (state, { payload }: PayloadAction<string>) => {
      state.promotions = state.promotions.filter(
        (promotion) => promotion._id !== payload
      )
    },
    addPromotion: (state, { payload }: PayloadAction<IPromotion>) => {
      state.promotions = [...state.promotions, payload]
    },
    updatePromotion: (state, { payload }: PayloadAction<IPromotion>) => {
      state.promotions = state.promotions.map((p) =>
        p._id === payload._id ? payload : p
      )
    },
    replacePromotion: (
      state,
      { payload }: PayloadAction<{ promotion: IPromotion; oldId: string }>
    ) => {
      state.promotions = state.promotions.map((p) =>
        p._id === payload.oldId ? payload.promotion : p
      )
    },
  },
})

export const {
  setPromotions,
  removePromotion,
  updatePromotion,
  replacePromotion,
  addPromotion,
} = promotionSlice.actions

export default promotionSlice.reducer
