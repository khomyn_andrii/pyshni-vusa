import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ITexts } from '../utils/types'

interface TextsState {
  texts: ITexts
}

const initialState: TextsState = {
  texts: {
    firstPhone: '',
    secondPhone: '',
    thirdPhone: '',
    startWorkTime: '',
    endWorkTime: '',
    address: '',
    secondAddress: '',
    seoText: '',
    facebook: '',
    instagram: '',
    telegram: '',
    contactsText: '',
    mapLink: '',
    promotionText: '',
    minOrderTime: 30,
    deliveryImage: '',
    deliveryText: '',
  },
}

export const textsSlice = createSlice({
  name: 'texts',
  initialState,
  reducers: {
    setTexts: (state, { payload }: PayloadAction<ITexts>) => {
      state.texts = payload
    },
  },
})

export const { setTexts } = textsSlice.actions

export default textsSlice.reducer
