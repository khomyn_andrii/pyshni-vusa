import axios, { AxiosRequestConfig } from 'axios'
import { store } from '../store'
import { setError } from '../store/error'

export default class AxiosService {
  instance = axios.create({
    baseURL: process.env.REACT_APP_SERVER,
  })

  get = async (url: string, params: AxiosRequestConfig | undefined) => {
    try {
      this.getToken()
      const response = await this.instance.get(url, params)
      if (response.status >= 400) {
        throw new Error('Failed to make request')
      }
      return response.data
    } catch (err) {
      store.dispatch(setError('Запрос не удался. Попробуйте еще раз'))
      throw Error(err)
    }
  }

  post = async (url: string, body: any, json = false) => {
    try {
      this.getToken()
      const response = await this.instance.post(url, body, {
        headers: {
          accept: 'application/json',
          'Content-Type': json ? 'application/json' : 'multipart/form-data',
        },
      })
      if (response.status >= 400) {
        throw new Error('Failed to make request')
      }
      return response.data
    } catch (err) {
      store.dispatch(
        setError('Запрос не удался. Проверьте ввод и попробуйте еще раз')
      )
      throw Error('Failed to make request')
    }
  }

  put = async (url: string, body: any, json = false) => {
    try {
      this.getToken()
      const response = await this.instance.put(url, body, {
        headers: {
          accept: 'application/json',
          'Content-Type': json ? 'application/json' : 'multipart/form-data',
        },
      })
      if (response.status >= 400) {
        throw new Error('Failed to make request')
      }
      return response.data
    } catch (err) {
      store.dispatch(
        setError(
          'Не удалось сделать изменения. Проверьте ввод и попробуйте еще раз'
        )
      )
      throw Error('Failed to make request')
    }
  }

  delete = async (url: string) => {
    try {
      this.getToken()
      const response = await this.instance.delete(url)
      if (response.status >= 400) {
        throw new Error('Failed to make request')
      }
      return response.data
    } catch (err) {
      store.dispatch(
        setError(
          'Не удалось сделать изменения. Проверьте ввод и попробуйте еще раз'
        )
      )
      throw Error('Failed to make request')
    }
  }

  getToken = async () => {
    try {
      const token = window.localStorage.getItem('token')

      this.instance.defaults.headers.common.Authorization = `Bearer ${token}`

      return token
    } catch (err) {
      return null
    }
  }

  postWithoutToken = async (url: string, body?: any) => {
    try {
      delete this.instance.defaults.headers.common.Authorization
      const response = await this.instance.post(url, body, {
        headers: {
          accept: 'application/json',
          'Content-Type': 'application/json',
        },
      })

      return response.data
    } catch (err) {
      return { hasError: true, textError: err }
    }
  }
}
