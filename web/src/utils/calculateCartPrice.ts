import { IOrder } from './types'

export const calculateCartPrice = (item: IOrder) => {
  return item.products.reduce(
    (sum, curr) =>
      (sum +=
        curr.quantity *
        (Number(curr.choosenSize.price) +
          (curr.additions.length &&
            curr.additions.reduce(
              (sum, curr) => (sum += curr.quantity * curr.price),
              0
            )))),
    0
  )
}
