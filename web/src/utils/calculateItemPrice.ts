import { ICartProduct } from './types'

export const calculateItemPrice = (item: ICartProduct) => {
  return Number(
    (
      (Number(item.choosenSize.price) +
        Number(
          item.additions.length > 0
            ? item.additions.reduce(
                (sum, curr) => (sum += curr.price * curr.quantity),
                0
              )
            : 0
        )) *
      item.quantity
    ).toFixed(2)
  )
}
