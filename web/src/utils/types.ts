export interface IProduct {
  _id: string
  title: string
  tags: IProductTags[]
  available: boolean
  image: string
  description: string
  sizes: ISize[]
  category: string
  weightTypes: WeightTypes
}

export enum WeightTypes {
  gram = 'gram',
  milliliter = 'milliliter',
}

export interface ISize {
  title: string
  weight: number
  diameter: number
  price: number
  _id: string
  [key: string]: string | number
}

export enum IProductTags {
  spicy = 'spicy',
  vegan = 'vegan',
  bbq = 'bbq',
}

export interface ICategory {
  _id: string
  title: string
  position: number
  showAdditions: boolean
}

export interface IPromotion {
  _id: string
  title: string
  description: string
  image: string
  carouselImage: string
  position: number
  showMain: boolean
}

export interface ICartProduct {
  product: IProduct
  quantity: number
  choosenSize: ISize
  additions: Array<IAddition & { quantity: number }>
  key: number
}

export interface IAdditionCategory {
  _id: string
  title: string
  position: number
  additions: IAddition[]
}

export interface IAddition {
  _id: string
  title: string
  weight: number
  price: number
  image: string
  category: string
  quantity: number
}

export interface IOrder {
  _id: string
  address: string
  changeFrom: string
  comment: string
  customerEmail: string
  customerName: string
  customerPhone: string
  date: Date
  deliveryType: 'delivery' | 'pickup'
  key: number
  paymentType: 'card' | 'cash'
  products: ICartProduct[]
  status: OrderStatusEnum
  sum: number
  callback: boolean
  createdAt: Date
}

export enum OrderStatusEnum {
  new = 'new',
  accepted = 'accepted',
  canceled = 'canceled',
  completed = 'completed',
}

export interface ITexts {
  firstPhone: string
  secondPhone: string
  thirdPhone: string
  startWorkTime: string
  endWorkTime: string
  address: string
  secondAddress: string
  seoText: string
  facebook: string
  instagram: string
  telegram: string
  contactsText: string
  mapLink: string
  promotionText: string
  minOrderTime: number
  deliveryImage: string
  deliveryText: string
}

export interface ICustomer {
  phone: string
  email: string
  name: string
  orderLastDate: Date
  orderCount: number
}
